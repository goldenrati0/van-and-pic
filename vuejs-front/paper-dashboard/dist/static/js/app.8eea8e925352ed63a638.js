webpackJsonp([0],[
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(64)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(44),
  /* template */
  __webpack_require__(129),
  /* scopeId */
  "data-v-49f597aa",
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Notifications_vue__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Notifications_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Notifications_vue__);


const NotificationStore = {
  state: [], // here the notifications will be added

  removeNotification(index) {
    this.state.splice(index, 1);
  },
  notify(notification) {
    this.state.push(notification);
  }
};

var NotificationsPlugin = {

  install(Vue) {
    Object.defineProperty(Vue.prototype, '$notifications', {
      get() {
        return NotificationStore;
      }
    });
    Vue.component('Notifications', __WEBPACK_IMPORTED_MODULE_0__Notifications_vue___default.a);
  }
};

/* harmony default export */ __webpack_exports__["a"] = (NotificationsPlugin);

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__SideBar_vue__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__SideBar_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__SideBar_vue__);


const SidebarStore = {
    showSidebar: true,
    sidebarLinks: [{ name: 'User Profile', icon: 'ti-user', path: '/app/profile' }, { name: 'Login', icon: 'ti-lock', path: '/app/login' }, { name: 'Register', icon: 'ti-star', path: '/app/register' }, { name: 'All Paintings', icon: 'ti-paint-bucket', path: '/app/paintings/1' }, { name: 'All Sculptures', icon: 'ti-hummer', path: '/app/sculptures/1' }, { name: 'My Paintings', icon: 'ti-paint-bucket', path: '/app/mypaintings/1' }, { name: 'My Sculptures', icon: 'ti-hummer', path: '/app/mysculptures/1' }],
    userLinks: [{ name: 'User Profile', icon: 'ti-user', path: '/app/profile' }, { name: 'All Paintings', icon: 'ti-paint-bucket', path: '/app/paintings/1' }, { name: 'All Sculptures', icon: 'ti-hummer', path: '/app/sculptures/1' }, { name: 'My Paintings', icon: 'ti-paint-bucket', path: '/app/mypaintings/1' }, { name: 'My Sculptures', icon: 'ti-hummer', path: '/app/mysculptures/1' }],
    guestLinks: [{ name: 'Login', icon: 'ti-lock', path: '/app/login' }, { name: 'Register', icon: 'ti-star', path: '/app/register' }, { name: 'All Paintings', icon: 'ti-paint-bucket', path: '/app/paintings/1' }, { name: 'All Sculptures', icon: 'ti-hummer', path: '/app/sculptures/1' }],
    adminLinks: [{ name: 'All Users', icon: 'ti-paint-bucket', path: '/app/users/1' }, { name: 'All Paintings', icon: 'ti-paint-bucket', path: '/app/paintings/1' }, { name: 'All Sculptures', icon: 'ti-hummer', path: '/app/sculptures/1' }],
    displaySidebar(value) {
        this.showSidebar = value;
    }
};

const SidebarPlugin = {

    install(Vue) {
        Vue.mixin({
            data() {
                return {
                    sidebarStore: SidebarStore
                };
            }
        });

        Object.defineProperty(Vue.prototype, '$sidebar', {
            get() {
                return this.$root.sidebarStore;
            }
        });
        Vue.component('side-bar', __WEBPACK_IMPORTED_MODULE_0__SideBar_vue___default.a);
    }
};

/* harmony default export */ __webpack_exports__["a"] = (SidebarPlugin);

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_UIComponents_Inputs_formGroupInput_vue__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_UIComponents_Inputs_formGroupInput_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_UIComponents_Inputs_formGroupInput_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_UIComponents_Dropdown_vue__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_UIComponents_Dropdown_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_UIComponents_Dropdown_vue__);



/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

const GlobalComponents = {
  install(Vue) {
    Vue.component('fg-input', __WEBPACK_IMPORTED_MODULE_0__components_UIComponents_Inputs_formGroupInput_vue___default.a);
    Vue.component('drop-down', __WEBPACK_IMPORTED_MODULE_1__components_UIComponents_Dropdown_vue___default.a);
  }
};

/* harmony default export */ __webpack_exports__["a"] = (GlobalComponents);

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_clickaway__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_clickaway___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_clickaway__);


/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

const GlobalDirectives = {
  install(Vue) {
    Vue.directive('click-outside', __WEBPACK_IMPORTED_MODULE_0_vue_clickaway__["directive"]);
  }
};

/* harmony default export */ __webpack_exports__["a"] = (GlobalDirectives);

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Dashboard_Layout_DashboardLayout_vue__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_Dashboard_Layout_DashboardLayout_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_Dashboard_Layout_DashboardLayout_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_GeneralViews_NotFoundPage_vue__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_GeneralViews_NotFoundPage_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_GeneralViews_NotFoundPage_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_components_Dashboard_Views_Overview_vue__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_src_components_Dashboard_Views_Overview_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_src_components_Dashboard_Views_Overview_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_src_components_Dashboard_Views_UserProfile_vue__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_src_components_Dashboard_Views_UserProfile_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_src_components_Dashboard_Views_UserProfile_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_src_components_Dashboard_Views_Notifications_vue__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_src_components_Dashboard_Views_Notifications_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_src_components_Dashboard_Views_Notifications_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_src_components_Dashboard_Views_Icons_vue__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_src_components_Dashboard_Views_Icons_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_src_components_Dashboard_Views_Icons_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_src_components_Dashboard_Views_Maps_vue__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_src_components_Dashboard_Views_Maps_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_src_components_Dashboard_Views_Maps_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_src_components_Dashboard_Views_Typography_vue__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_src_components_Dashboard_Views_Typography_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_src_components_Dashboard_Views_Typography_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_src_components_Dashboard_Views_TableList_vue__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_src_components_Dashboard_Views_TableList_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_src_components_Dashboard_Views_TableList_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_src_components_Dashboard_Views_Register_vue__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_src_components_Dashboard_Views_Register_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_src_components_Dashboard_Views_Register_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_src_components_Dashboard_Views_Login_vue__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_src_components_Dashboard_Views_Login_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_src_components_Dashboard_Views_Login_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_src_components_Dashboard_Views_List_vue__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_src_components_Dashboard_Views_List_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_src_components_Dashboard_Views_List_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_src_components_Dashboard_Views_UserList_vue__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_src_components_Dashboard_Views_UserList_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_src_components_Dashboard_Views_UserList_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_src_components_Dashboard_Views_AddPhoto_vue__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_src_components_Dashboard_Views_AddPhoto_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_src_components_Dashboard_Views_AddPhoto_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_src_components_Dashboard_Views_AddSculpt_vue__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_src_components_Dashboard_Views_AddSculpt_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_src_components_Dashboard_Views_AddSculpt_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_src_components_Dashboard_Views_ListUsers_vue__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_src_components_Dashboard_Views_ListUsers_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_src_components_Dashboard_Views_ListUsers_vue__);

// GeneralViews


// Admin pages














const routes = [{
  path: '/',
  component: __WEBPACK_IMPORTED_MODULE_0__components_Dashboard_Layout_DashboardLayout_vue___default.a,
  redirect: '/app/login'
}, {
  path: '/app',
  component: __WEBPACK_IMPORTED_MODULE_0__components_Dashboard_Layout_DashboardLayout_vue___default.a,
  redirect: '/app/profile',
  children: [{
    path: 'profile',
    name: 'profile',
    component: __WEBPACK_IMPORTED_MODULE_3_src_components_Dashboard_Views_UserProfile_vue___default.a
  }, {
    path: 'profile/:id',
    name: 'profileid',
    component: __WEBPACK_IMPORTED_MODULE_3_src_components_Dashboard_Views_UserProfile_vue___default.a
  }, {
    path: 'add_painting',
    name: 'add painting',
    component: __WEBPACK_IMPORTED_MODULE_13_src_components_Dashboard_Views_AddPhoto_vue___default.a
  }, {
    path: 'add_sculpture',
    name: 'add sculpture',
    component: __WEBPACK_IMPORTED_MODULE_14_src_components_Dashboard_Views_AddSculpt_vue___default.a
  }, {
    path: 'icons',
    name: 'icons',
    component: __WEBPACK_IMPORTED_MODULE_5_src_components_Dashboard_Views_Icons_vue___default.a
  }, {
    path: 'paintings/:id',
    name: 'paintings',
    component: __WEBPACK_IMPORTED_MODULE_11_src_components_Dashboard_Views_List_vue___default.a
  }, {
    path: 'mypaintings/:id',
    name: 'mypaintings',
    component: __WEBPACK_IMPORTED_MODULE_12_src_components_Dashboard_Views_UserList_vue___default.a
  }, {
    path: 'sculptures/:id',
    name: 'sculptures',
    component: __WEBPACK_IMPORTED_MODULE_11_src_components_Dashboard_Views_List_vue___default.a
  }, {
    path: 'mysculptures/:id',
    name: 'mysculptures',
    component: __WEBPACK_IMPORTED_MODULE_12_src_components_Dashboard_Views_UserList_vue___default.a
  }, {
    path: 'login',
    name: 'login',
    component: __WEBPACK_IMPORTED_MODULE_10_src_components_Dashboard_Views_Login_vue___default.a
  }, {
    path: 'register',
    name: 'register',
    component: __WEBPACK_IMPORTED_MODULE_9_src_components_Dashboard_Views_Register_vue___default.a
  }, { path: 'users/:id', name: 'user_list', component: __WEBPACK_IMPORTED_MODULE_15_src_components_Dashboard_Views_ListUsers_vue___default.a }]
}, { path: '*', component: __WEBPACK_IMPORTED_MODULE_1__components_GeneralViews_NotFoundPage_vue___default.a }];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

/* harmony default export */ __webpack_exports__["a"] = (routes);

/***/ }),
/* 9 */,
/* 10 */,
/* 11 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 12 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(59)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(16),
  /* template */
  __webpack_require__(124),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 14 */,
/* 15 */,
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__TopNavbar_vue__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__TopNavbar_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__TopNavbar_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ContentFooter_vue__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ContentFooter_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__ContentFooter_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Content_vue__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Content_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__Content_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    data() {
        return {
            loggedIn: false
        };
    },
    components: {
        TopNavbar: __WEBPACK_IMPORTED_MODULE_0__TopNavbar_vue___default.a,
        ContentFooter: __WEBPACK_IMPORTED_MODULE_1__ContentFooter_vue___default.a,
        DashboardContent: __WEBPACK_IMPORTED_MODULE_2__Content_vue___default.a
    },
    methods: {
        toggleSidebar() {
            if (this.$sidebar.showSidebar) {
                this.$sidebar.displaySidebar(false);
            }
        }
    },
    created() {
        if (localStorage.getItem('id') > 0) {
            this.loggedIn = 0;
            if (localStorage.getItem('id') == 999999) {
                this.loggedIn = 1;
            }
        } else {
            this.loggedIn = -1;
        }
    }
});

/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    computed: {
        routeName() {
            const { name } = this.$route;
            return this.capitalizeFirstLetter(name);
        }
    },
    data() {
        return {
            name: '',
            id: '',
            activeNotifications: false
        };
    },
    computed: {
        getID() {
            return localStorage.getItem('id');
        }
    },
    methods: {
        logOut() {
            localStorage.clear();
            this.$router.push('/app/login');
            this.$router.go({
                path: this.$router.path,
                query: {
                    t: +new Date()
                }
            });
        }
    },
    created() {

        if (localStorage.getItem('id')) {
            this.id = localStorage.getItem('id');
            console.log("Working");

            if (this.id != 999999) {
                this.$http.get("http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/" + this.id).then(response => {
                    // success callback
                    this.name = "Logged in as: " + response.body.name;
                    console.log("Name Set");
                }, response => {
                    // error callback
                });
            } else {
                this.name = "Logged in as: Administrator";
            }
        }
    }
});

/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    data() {
        return {
            imgURL: 'NONE',
            baseAPI: 'https://api.cloudinary.com/v1_1/dbnoalpc0',
            code: 'cse370',
            painting: {},
            pic: {},
            url: '',
            id: '',
            type: ['', 'info', 'success', 'warning', 'danger'],
            notifications: {
                topCenter: false
            },
            cloudinary: {
                uploadPreset: 'cse370',
                apiKey: '275388512423949',
                cloudName: 'dbnoalpc0'
            },
            thumb: {
                url: ''
            },
            thumbs: []
        };
    },
    computed: {
        clUrl: function () {
            return `https://api.cloudinary.com/v1_1/${this.cloudinary.cloudName}/upload`;
        }
    },
    methods: {
        notifyVue(verticalAlign, horizontalAlign) {
            var color = Math.floor(Math.random() * 4 + 1);
            this.$notifications.notify({
                message: '<b>Painting Successfully Added!!</b>',
                icon: 'ti-check',
                horizontalAlign: horizontalAlign,
                verticalAlign: verticalAlign,
                type: this.type[2]
            });
        },

        uploadPainting() {
            this.$http.post("http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/paintings", { artist_id: this.id, description: this.painting.description, name: this.painting.name, photo: this.painting.photo }).then(response => {
                // success callback
                console.log(response.body);
                this.notifyVue('top', 'right');
                this.$router.push('/app/mypaintings/1');
            }, response => {
                // error callback
            });
        },

        upload(file) {
            const formData = new FormData();
            formData.append('file', file[0]);
            formData.append('upload_preset', this.cloudinary.uploadPreset);
            formData.append('tags', 'gs-vue,gs-vue-uploaded');
            this.painting.photo = 'Please wait...';
            this.$http.post(this.clUrl, formData).then(response => {
                // success callback
                this.painting.photo = response.body.url;
                this.imgURL = response.body.url;
                console.log(response.body);
            }, response => {
                // error callback
            });
            //            .then(res => {
            //                    console.log(res.body)
            //                    this.thumbs.unshift({
            //                        url: res.data.secure_url
            //                    });
            //
            //
            //                })
        }

    },
    created() {
        this.id = localStorage.getItem('id');
    },
    components: {
        PaperNotification: __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default.a
    }
});

/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    data() {
        return {
            imgURL: '',
            baseAPI: 'https://api.cloudinary.com/v1_1/dbnoalpc0',
            code: 'cse370',
            painting: {},
            pic: {},
            url: '',
            id: '',
            type: ['', 'info', 'success', 'warning', 'danger'],
            notifications: {
                topCenter: false
            },
            cloudinary: {
                uploadPreset: 'cse370',
                apiKey: '275388512423949',
                cloudName: 'dbnoalpc0'
            },
            thumb: {
                url: ''
            },
            thumbs: []
        };
    },
    computed: {
        clUrl: function () {
            return `https://api.cloudinary.com/v1_1/${this.cloudinary.cloudName}/upload`;
        }
    },
    methods: {
        notifyVue(verticalAlign, horizontalAlign) {
            var color = Math.floor(Math.random() * 4 + 1);
            this.$notifications.notify({
                message: '<b>Sculpture Successfully Added!!</b>',
                icon: 'ti-check',
                horizontalAlign: horizontalAlign,
                verticalAlign: verticalAlign,
                type: this.type[2]
            });
        },
        onFileChange(e) {
            let files = e.target.files || e.dataTransfer.files;
            if (!files.length) return;
            this.createImage(files[0]);
        },
        createImage(file) {
            let image = new Image();
            let reader = new FileReader();
            let vm = this;

            reader.onload = e => {
                vm.painting.photo = e.target.result;
            };
            reader.readAsDataURL(file);
        },
        uploadPainting() {
            this.$http.post("http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/sculptures", { artist_id: this.id, description: this.painting.description, name: this.painting.name, photo: this.painting.photo }).then(response => {
                // success callback
                this.notifyVue('top', 'right');
                this.$router.push('/app/mysculptures/1');
            }, response => {
                // error callback

            });
        },
        upload(file) {
            const formData = new FormData();
            formData.append('file', file[0]);
            formData.append('upload_preset', this.cloudinary.uploadPreset);
            formData.append('tags', 'gs-vue,gs-vue-uploaded');
            this.$http.post(this.clUrl, formData).then(response => {
                // success callback
                this.painting.photo = response.body.url;
                this.imgURL = response.body.url;
                console.log(response.body);
            }, response => {
                // error callback
            });
            //            .then(res => {
            //                    console.log(res.body)
            //                    this.thumbs.unshift({
            //                        url: res.data.secure_url
            //                    });
            //
            //
            //                })
        }

    },
    created() {
        this.id = localStorage.getItem('id');
    },
    components: {
        PaperNotification: __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default.a
    }
});

/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),
/* 24 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({

    data() {
        return {
            editMode: false,
            tmpLen: 0,
            noPage: 1,
            currentPage: 1,
            objs: '',
            selected: '',
            isAdmin: false
        };
    },

    methods: {
        deletePhoto(iid) {
            if (this.selected == "Painting") {
                this.$http.delete('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/paintings/' + iid).then(response => {
                    // success callback
                    this.getPaintings();

                    this.notifyVue('top', 'right', 'Painting Successfully Deleted!!');
                }, response => {
                    // error callback
                });
            } else if (this.selected == "Sculpture") {
                this.$http.delete('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/sculptures/' + iid).then(response => {
                    // success callback
                    this.getSculps();
                    this.notifyVue('top', 'right', 'Sculpture Scuccessfully Deleted!!');
                }, response => {
                    // error callback
                });
            }
            //                this.$router.go({
            //                    path: this.$router.path,
            //                    query: {
            //                        t: + new Date()
            //                    }
            //                })
        },
        editPhoto(obj) {
            this.painting = obj;
            this.editMode = true;
        },
        edit() {
            if (this.selected == "Painting") {
                console.log(this.painting);

                this.$http.put('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/paintings/' + this.painting.id, { name: this.painting.name, description: this.painting.description }).then(response => {
                    // success callback
                    console.log(response.body);
                    this.getPaintings();
                }, response => {
                    // error callback
                });

                this.editMode = false;
                this.notifyVue('top', 'right', 'Successfully Edited!!');
            } else if (this.selected == "Sculpture") {
                console.log(this.painting);

                this.$http.put('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/sculptures/' + this.painting.id, { name: this.painting.name, description: this.painting.description }).then(response => {
                    // success callback
                    console.log(response.body);
                    this.getSculps();
                }, response => {
                    // error callback
                });

                this.editMode = false;
                this.notifyVue('top', 'right', 'Successfully Edited!!');
            }
            //                this.$router.go({
            //                    path: this.$router.path,
            //                    query: {
            //                        t: + new Date()
            //                    }
            //                })
        },
        getPaintings() {
            this.$http.get('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/paintings?page=' + this.currentPage).then(response => {
                this.noPage = response.body.total_pages;
                this.currentPage = response.body.page;
                this.objs = response.body.objects;
                console.log("Paintings");
                console.log(this.objs);
            }, response => {
                // error callback
            });
        },
        getSculps() {
            this.$http.get('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/sculptures?page=' + this.currentPage).then(response => {
                this.noPage = response.body.total_pages;
                this.currentPage = response.body.page;
                this.objs = response.body.objects;
                console.log("Sculps");
                console.log(this.objs);
            }, response => {
                // error callback
            });
        }
    },

    created() {
        this.currentPage = this.$route.params.id;
        if (localStorage.getItem('id') == 999999) {
            this.isAdmin = true;
        }
        if (this.$route.path == '/app/paintings/1') {
            this.selected = 'Painting';
            this.getPaintings();
        } else if (this.$route.path == '/app/sculptures/1') {
            this.selected = 'Sculpture';
            this.getSculps();
        }
    },
    watch: {
        '$route'(to, from) {
            // react to route changes...
            this.currentPage = this.$route.params.id;

            if (to.path.startsWith("/app/paintings/")) {
                this.selected = 'Painting';
                this.getPaintings();
            } else if (to.path.startsWith("/app/sculptures/")) {
                this.selected = 'Sculpture';
                this.getSculps();
            }
        }
    }
});

/***/ }),
/* 25 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({

    data() {
        return {
            editMode: false,
            tmpLen: 0,
            noPage: 1,
            currentPage: 1,
            objs: '',
            selected: '',
            isAdmin: false,
            type: ['', 'info', 'success', 'warning', 'danger'],
            notifications: {
                topCenter: false
            }
        };
    },

    methods: {
        deletePhoto(iid) {

            this.$http.delete('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/' + iid).then(response => {
                // success callback
                this.getUsers();
                this.notifyVue('top', 'right', 'User Scuccessfully Deleted!!');
            }, response => {
                // error callback
            });
        },
        notifyVue(verticalAlign, horizontalAlign, msg) {
            var color = Math.floor(Math.random() * 4 + 1);
            this.$notifications.notify({
                message: '<b>' + msg + '</b>',
                icon: 'ti-check',
                horizontalAlign: horizontalAlign,
                verticalAlign: verticalAlign,
                type: this.type[2]
            });
        },
        getUsers() {
            this.$http.get('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist?page=' + this.currentPage).then(response => {
                this.noPage = response.body.total_pages;
                this.currentPage = response.body.page;
                this.objs = response.body.objects;
            }, response => {
                // error callback
            });
        }

    },

    created() {
        this.currentPage = this.$route.params.id;
        if (localStorage.getItem('id') == 999999) {
            this.isAdmin = true;
        }
        this.getUsers();
    },
    watch: {
        '$route'(to, from) {
            // react to route changes...
            this.currentPage = this.$route.params.id;

            if (to.path.startsWith("/app/users/")) {
                this.getUsers();
            }
        }
    },
    components: {
        PaperNotification: __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default.a
    }
});

/***/ }),
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    data() {
        return {
            type: ['', 'info', 'success', 'warning', 'danger'],
            notifications: {
                topCenter: false
            },
            status: ''
        };
    },
    methods: {
        notifyVue(verticalAlign, horizontalAlign) {
            var color = Math.floor(Math.random() * 4 + 1);
            this.$notifications.notify({
                message: '<b>Successfully logged in!!</b>',
                icon: 'ti-check',
                horizontalAlign: horizontalAlign,
                verticalAlign: verticalAlign,
                type: this.type[2]
            });
        },
        notifyVue2(verticalAlign, horizontalAlign) {
            var color = Math.floor(Math.random() * 4 + 1);
            this.$notifications.notify({
                message: '<b>Invalid Credentials!!</b>',
                icon: 'ti-cross',
                horizontalAlign: horizontalAlign,
                verticalAlign: verticalAlign,
                type: this.type[4]
            });
        },
        login() {
            if (this.email == 'admin' && this.password == 'admin') {
                localStorage.setItem('id', 999999);
                this.$router.push('/app/users/1');
                this.$router.go({
                    path: this.$router.path,
                    query: {
                        t: +new Date()
                    }
                });
                this.notifyVue('top', 'right');
            } else {

                this.$http.post('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/login/', {
                    email: this.email,
                    password: this.password
                }).then(response => {
                    localStorage.setItem('id', response.body.id);
                    this.$router.push('/app/profile');
                    this.$router.go({
                        path: this.$router.path,
                        query: {
                            t: +new Date()
                        }
                    });
                    this.notifyVue('top', 'right');
                }, response => {
                    this.notifyVue2('top', 'right');
                });
            }
        }

    },
    components: {
        PaperNotification: __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default.a
    }
});

/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  mounted() {
    var myLatlng = new window.google.maps.LatLng(40.748817, -73.985428);
    var mapOptions = {
      zoom: 13,
      center: myLatlng,
      scrollwheel: false, // we disable de scroll over the map, it is a really annoing when you scroll through page
      styles: [{
        'featureType': 'water',
        'stylers': [{ 'saturation': 43 }, { 'lightness': -11 }, { 'hue': '#0088ff' }]
      }, {
        'featureType': 'road',
        'elementType': 'geometry.fill',
        'stylers': [{ 'hue': '#ff0000' }, { 'saturation': -100 }, { 'lightness': 99 }]
      }, {
        'featureType': 'road',
        'elementType': 'geometry.stroke',
        'stylers': [{ 'color': '#808080' }, { 'lightness': 54 }]
      }, {
        'featureType': 'landscape.man_made',
        'elementType': 'geometry.fill',
        'stylers': [{ 'color': '#ece2d9' }]
      }, {
        'featureType': 'poi.park',
        'elementType': 'geometry.fill',
        'stylers': [{ 'color': '#ccdca1' }]
      }, {
        'featureType': 'road',
        'elementType': 'labels.text.fill',
        'stylers': [{ 'color': '#767676' }]
      }, {
        'featureType': 'road',
        'elementType': 'labels.text.stroke',
        'stylers': [{ 'color': '#ffffff' }]
      }, { 'featureType': 'poi', 'stylers': [{ 'visibility': 'off' }] }, {
        'featureType': 'landscape.natural',
        'elementType': 'geometry.fill',
        'stylers': [{ 'visibility': 'on' }, { 'color': '#b8cb93' }]
      }, { 'featureType': 'poi.park', 'stylers': [{ 'visibility': 'on' }] }, {
        'featureType': 'poi.sports_complex',
        'stylers': [{ 'visibility': 'on' }]
      }, { 'featureType': 'poi.medical', 'stylers': [{ 'visibility': 'on' }] }, {
        'featureType': 'poi.business',
        'stylers': [{ 'visibility': 'simplified' }]
      }]

    };
    var map = new window.google.maps.Map(document.getElementById('map'), mapOptions);

    var marker = new window.google.maps.Marker({
      position: myLatlng,
      title: 'Hello World!'
    });

    // To add the marker to the map, call setMap();
    marker.setMap(map);
  }
});

/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data() {
    return {
      type: ['', 'info', 'success', 'warning', 'danger'],
      notifications: {
        topCenter: false
      }
    };
  },
  components: {
    PaperNotification: __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default.a
  },
  methods: {
    notifyVue(verticalAlign, horizontalAlign) {
      var color = Math.floor(Math.random() * 4 + 1);
      this.$notifications.notify({
        message: 'Welcome to <b>Paper Dashboard</b> - a beautiful freebie for every web developer.',
        icon: 'ti-gift',
        horizontalAlign: horizontalAlign,
        verticalAlign: verticalAlign,
        type: this.type[color]
      });
    }
  }
});

/***/ }),
/* 29 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_UIComponents_Cards_StatsCard_vue__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_UIComponents_Cards_StatsCard_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_UIComponents_Cards_StatsCard_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_UIComponents_Cards_ChartCard_vue__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_components_UIComponents_Cards_ChartCard_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_components_UIComponents_Cards_ChartCard_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    StatsCard: __WEBPACK_IMPORTED_MODULE_0_components_UIComponents_Cards_StatsCard_vue___default.a,
    ChartCard: __WEBPACK_IMPORTED_MODULE_1_components_UIComponents_Cards_ChartCard_vue___default.a
  },
  /**
   * Chart data used to render stats, charts. Should be replaced with server data
   */
  data() {
    return {
      statsCards: [{
        type: 'warning',
        icon: 'ti-server',
        title: 'Capacity',
        value: '105GB',
        footerText: 'Updated now',
        footerIcon: 'ti-reload'
      }, {
        type: 'success',
        icon: 'ti-wallet',
        title: 'Revenue',
        value: '$1,345',
        footerText: 'Last day',
        footerIcon: 'ti-calendar'
      }, {
        type: 'danger',
        icon: 'ti-pulse',
        title: 'Errors',
        value: '23',
        footerText: 'In the last hour',
        footerIcon: 'ti-timer'
      }, {
        type: 'info',
        icon: 'ti-twitter-alt',
        title: 'Followers',
        value: '+45',
        footerText: 'Updated now',
        footerIcon: 'ti-reload'
      }],
      usersChart: {
        data: {
          labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
          series: [[287, 385, 490, 562, 594, 626, 698, 895, 952], [67, 152, 193, 240, 387, 435, 535, 642, 744], [23, 113, 67, 108, 190, 239, 307, 410, 410]]
        },
        options: {
          low: 0,
          high: 1000,
          showArea: true,
          height: '245px',
          axisX: {
            showGrid: false
          },
          lineSmooth: this.$Chartist.Interpolation.simple({
            divisor: 3
          }),
          showLine: true,
          showPoint: false
        }
      },
      activityChart: {
        data: {
          labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
          series: [[542, 543, 520, 680, 653, 753, 326, 434, 568, 610, 756, 895], [230, 293, 380, 480, 503, 553, 600, 664, 698, 710, 736, 795]]
        },
        options: {
          seriesBarDistance: 10,
          axisX: {
            showGrid: false
          },
          height: '245px'
        }
      },
      preferencesChart: {
        data: {
          labels: ['62%', '32%', '6%'],
          series: [62, 32, 6]
        },
        options: {}
      }

    };
  }
});

/***/ }),
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({

    data() {
        return {
            user: {}
        };
    },
    methods: {
        notifyVue(verticalAlign, horizontalAlign) {
            var color = Math.floor(Math.random() * 4 + 1);
            this.$notifications.notify({
                message: '<b>Successfully Registered!!</b>',
                icon: 'ti-check',
                horizontalAlign: horizontalAlign,
                verticalAlign: verticalAlign,
                type: this.type[2]
            });
        },
        register() {
            this.$http.post("http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist", this.user).then(response => {
                // success callback
                this.$router.push('/app/login');
                this.notifyVue('top', 'right');
            }, response => {
                // error callback
            });
        },
        components: {
            PaperNotification: __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default.a
        }

    }
});

/***/ }),
/* 31 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_UIComponents_PaperTable_vue__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_components_UIComponents_PaperTable_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_components_UIComponents_PaperTable_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


const tableColumns = ['Id', 'Name', 'Salary', 'Country', 'City'];
const tableData = [{
  id: 1,
  name: 'Dakota Rice',
  salary: '$36.738',
  country: 'Niger',
  city: 'Oud-Turnhout'
}, {
  id: 2,
  name: 'Minerva Hooper',
  salary: '$23,789',
  country: 'Curaçao',
  city: 'Sinaai-Waas'
}, {
  id: 3,
  name: 'Sage Rodriguez',
  salary: '$56,142',
  country: 'Netherlands',
  city: 'Baileux'
}, {
  id: 4,
  name: 'Philip Chaney',
  salary: '$38,735',
  country: 'Korea, South',
  city: 'Overland Park'
}, {
  id: 5,
  name: 'Doris Greene',
  salary: '$63,542',
  country: 'Malawi',
  city: 'Feldkirchen in Kärnten'
}];

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    PaperTable: __WEBPACK_IMPORTED_MODULE_0_components_UIComponents_PaperTable_vue___default.a
  },
  data() {
    return {
      table1: {
        title: 'Stripped Table',
        subTitle: 'Here is a subtitle for this table',
        columns: [...tableColumns],
        data: [...tableData]
      },
      table2: {
        title: 'Table on Plain Background',
        subTitle: 'Here is a subtitle for this table',
        columns: [...tableColumns],
        data: [...tableData]
      }
    };
  }
});

/***/ }),
/* 32 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),
/* 33 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({

    data() {
        return {
            editMode: false,
            id: '',
            tmpLen: 0,
            noPage: 1,
            currentPage: 1,
            objs: '',
            selected: '',
            type: ['', 'info', 'success', 'warning', 'danger'],
            notifications: {
                topCenter: false
            },
            painting: {}
        };
    },

    methods: {
        notifyVue(verticalAlign, horizontalAlign, msg) {
            var color = Math.floor(Math.random() * 4 + 1);
            this.$notifications.notify({
                message: '<b>' + msg + '</b>',
                icon: 'ti-check',
                horizontalAlign: horizontalAlign,
                verticalAlign: verticalAlign,
                type: this.type[2]
            });
        },
        deletePhoto(iid) {
            if (this.selected == "Painting") {
                this.$http.delete('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/paintings/' + iid).then(response => {
                    // success callback
                    this.getPaintings();

                    this.notifyVue('top', 'right', 'Painting Successfully Deleted!!');
                }, response => {
                    // error callback
                });
            } else if (this.selected == "Sculpture") {
                this.$http.delete('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/sculptures/' + iid).then(response => {
                    // success callback
                    this.getSculps();
                    this.notifyVue('top', 'right', 'Sculpture Scuccessfully Deleted!!');
                }, response => {
                    // error callback
                });
            }
            //                this.$router.go({
            //                    path: this.$router.path,
            //                    query: {
            //                        t: + new Date()
            //                    }
            //                })
        },
        editPhoto(obj) {
            this.painting = obj;
            this.editMode = true;
        },
        edit() {
            if (this.selected == "Painting") {
                console.log(this.painting);

                this.$http.put('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/paintings/' + this.painting.id, { name: this.painting.name, description: this.painting.description }).then(response => {
                    // success callback
                    console.log(response.body);
                    this.getPaintings();
                }, response => {
                    // error callback
                });

                this.editMode = false;
                this.notifyVue('top', 'right', 'Successfully Edited!!');
            } else if (this.selected == "Sculpture") {
                console.log(this.painting);

                this.$http.put('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/sculptures/' + this.painting.id, { name: this.painting.name, description: this.painting.description }).then(response => {
                    // success callback
                    console.log(response.body);
                    this.getSculps();
                }, response => {
                    // error callback
                });

                this.editMode = false;
                this.notifyVue('top', 'right', 'Successfully Edited!!');
            }
            //                this.$router.go({
            //                    path: this.$router.path,
            //                    query: {
            //                        t: + new Date()
            //                    }
            //                })
        },
        getPaintings() {
            this.$http.get('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/' + this.id).then(response => {
                this.objs = response.body.painted;
            }, response => {
                // error callback
            });
        },
        getSculps() {
            this.$http.get('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/' + this.id).then(response => {
                this.objs = response.body.sculpted;
            }, response => {
                // error callback
            });
        }
    },

    created() {
        this.id = localStorage.getItem('id');
        if (this.$route.path == '/app/mypaintings/1') {
            this.selected = 'Painting';
            this.getPaintings();
        } else if (this.$route.path == '/app/mysculptures/1') {
            this.selected = 'Sculpture';
            this.getSculps();
        }
    },
    watch: {
        '$route'(to, from) {
            // react to route changes...
            this.currentPage = this.$route.params.id;

            if (to.path.startsWith("/app/mypaintings/")) {
                this.selected = 'Painting';
                this.getPaintings();
            } else if (to.path.startsWith("/app/mysculptures/")) {
                this.selected = 'Sculpture';
                this.getSculps();
            }
        }
    },
    components: {
        PaperNotification: __WEBPACK_IMPORTED_MODULE_0_src_components_UIComponents_NotificationPlugin_Notification_vue___default.a
    }
});

/***/ }),
/* 34 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UserProfile_EditProfileForm_vue__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UserProfile_EditProfileForm_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__UserProfile_EditProfileForm_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__UserProfile_UserCard_vue__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__UserProfile_UserCard_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__UserProfile_UserCard_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__UserProfile_MembersCard_vue__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__UserProfile_MembersCard_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__UserProfile_MembersCard_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__UserProfile_ListByID_vue__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__UserProfile_ListByID_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__UserProfile_ListByID_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    EditProfileForm: __WEBPACK_IMPORTED_MODULE_0__UserProfile_EditProfileForm_vue___default.a,
    UserCard: __WEBPACK_IMPORTED_MODULE_1__UserProfile_UserCard_vue___default.a,
    MembersCard: __WEBPACK_IMPORTED_MODULE_2__UserProfile_MembersCard_vue___default.a,
    List: __WEBPACK_IMPORTED_MODULE_3__UserProfile_ListByID_vue___default.a
  }
});

/***/ }),
/* 35 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data() {
        return {
            pubID: '',
            pub: false,
            id: '',
            obj: '',
            user: {
                email: '',
                name: '',
                phone: '',
                profile_photo: ''
            }
        };
    },
    methods: {
        updateProfile() {
            this.$http.put("http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/" + this.id, this.user).then(response => {
                // success callback
                console.log(response.body);
                this.$router.go({
                    path: this.$router.path,
                    query: {
                        t: +new Date()
                    }
                });
            }, response => {
                // error callback
            });
        },
        getUser() {
            this.$http.get("http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/" + this.id).then(response => {
                // success callback
                this.user.email = response.body.email;
                this.user.name = response.body.name;
                this.user.phone = response.body.phone;
                this.user.profile_photo = response.body.profile_photo;
                this.obj = response.body;
            }, response => {
                // error callback
            });
        },
        getUserID() {
            this.$http.get("http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/" + this.pubID).then(response => {
                // success callback
                this.user.email = response.body.email;
                this.user.name = response.body.name;
                this.user.phone = response.body.phone;
                this.user.profile_photo = response.body.profile_photo;
                this.obj = response.body;
            }, response => {
                // error callback
            });
        }
    },
    watch: {
        '$route'(to, from) {
            // react to route changes...


            if (to.path.startsWith("/app/profile/")) {
                this.pub = true;
                this.pubID = this.$route.params.id;
                this.getUserID();
            } else {
                this.getUser();
                this.pub = false;
            }
        }
    },
    created() {
        this.id = localStorage.getItem('id');
        if (this.$route.path.length > 13) {
            this.pub = true;
            this.pubID = this.$route.params.id;
            this.getUserID();
        } else {
            this.getUser();
            this.pub = false;
        }
    }
});

/***/ }),
/* 36 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({

    data() {
        return {
            tmpLen: 0,
            noPage: 1,
            currentPage: 1,
            objs: '',
            selected: '',
            paints: '',
            sculps: '',
            pubID: '',
            publ: true
        };
    },

    methods: {
        getPaintings() {
            this.$http.get('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/' + this.pubID).then(response => {
                this.paints = response.body.painted;
            }, response => {
                // error callback
            });
        },

        getSculps() {
            this.$http.get('http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/' + this.pubID).then(response => {
                this.sculps = response.body.sculpted;
            }, response => {
                // error callback
            });
        }

    },

    created() {
        this.pubID = this.$route.params.id;
        this.getPaintings();
        this.getSculps();
    },
    watch: {
        '$route'(to, from) {
            // react to route changes...


            if (to.path.startsWith("/app/profile/")) {

                this.pubID = this.$route.params.id;
                this.getPaintings();
                this.getSculps();
            } else {
                this.pubID = localStorage.getItem('id');
                this.getPaintings();
                this.getSculps();
            }
        }
    }
});

/***/ }),
/* 37 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data() {
    return {
      title: 'Team members',
      members: [{
        image: 'static/img/faces/face-0.jpg',
        name: 'Dj Khaled',
        status: 'Offline'
      }, {
        image: 'static/img/faces/face-1.jpg',
        name: 'Creative Tim',
        status: 'Available'
      }, {
        image: 'static/img/faces/face-1.jpg',
        name: 'Flume',
        status: 'Busy'
      }]
    };
  },
  methods: {
    getStatusClass(status) {
      switch (status) {
        case 'Offline':
          return 'text-muted';
        case 'Available':
          return 'text-success';
        case 'Busy':
          return 'text-danger';
        default:
          return 'text-success';
      }
    }
  }
});

/***/ }),
/* 38 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data() {
        return {
            details: [{
                title: '',
                subTitle: 'Paintings'
            }, {
                title: '',
                subTitle: 'Sculptures'
            }, {
                title: '',
                subTitle: 'Joined'
            }],
            gender: '',
            name: '',
            id: '',
            profile: '',
            pub: false,
            paints: '',
            sculps: ''
        };
    },
    methods: {
        getClasses(index) {
            var remainder = index % 3;
            if (remainder === 0) {
                return 'col-md-3 col-md-offset-1';
            } else if (remainder === 2) {
                return 'col-md-4';
            } else {
                return 'col-md-3';
            }
        },
        getUser() {
            this.$http.get("http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/" + this.id).then(response => {
                // success callback
                this.name = response.body.name;
                this.gender = response.body.gender;
                this.details[1].title = response.body.sculpted.length;
                this.details[0].title = response.body.painted.length;
                this.details[2].title = 2017;
                this.profile = response.body.profile_photo;
            }, response => {
                // error callback
            });
        },
        getUserID() {
            this.$http.get("http://ec2-13-228-73-130.ap-southeast-1.compute.amazonaws.com:5000/api/v1/artist/" + this.pubID).then(response => {
                // success callback
                this.name = response.body.name;
                this.gender = response.body.gender;
                this.details[1].title = response.body.sculpted.length;
                this.details[0].title = response.body.painted.length;
                this.details[2].title = 2017;
                this.profile = response.body.profile_photo;
                this.paints = response.body.painted;
                this.sculps = response.body.sculpted;
            }, response => {
                // error callback
            });
        }
    },
    watch: {
        '$route'(to, from) {
            // react to route changes...


            if (to.path.startsWith("/app/profile/")) {
                this.pub = true;
                this.pubID = this.$route.params.id;
                this.getUserID();
            } else {
                this.getUser();
                this.pub = false;
            }
        }
    },
    created() {
        this.id = localStorage.getItem('id');
        if (this.$route.path.length > 13) {
            this.pub = true;
            this.pubID = this.$route.params.id;
            this.getUserID();
        } else {
            this.getUser();
            this.pub = false;
        }
    }
});

/***/ }),
/* 39 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'chart-card',
  props: {
    footerText: {
      type: String,
      default: ''
    },
    headerTitle: {
      type: String,
      default: 'Chart title'
    },
    chartType: {
      type: String,
      default: 'Line' // Line | Pie | Bar
    },
    chartOptions: {
      type: Object,
      default: () => {
        return {};
      }
    },
    chartData: {
      type: Object,
      default: () => {
        return {
          labels: [],
          series: []
        };
      }
    }
  },
  data() {
    return {
      chartId: 'no-id'
    };
  },
  methods: {
    /***
     * Initializes the chart by merging the chart options sent via props and the default chart options
     */
    initChart() {
      var chartIdQuery = `#${this.chartId}`;
      this.$Chartist[this.chartType](chartIdQuery, this.chartData, this.chartOptions);
    },
    /***
     * Assigns a random id to the chart
     */
    updateChartId() {
      var currentTime = new Date().getTime().toString();
      var randomInt = this.getRandomInt(0, currentTime);
      this.chartId = `div_${randomInt}`;
    },
    getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
  },
  mounted() {
    this.updateChartId();
    this.$nextTick(this.initChart);
  }
});

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'stats-card'
});

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    title: String,
    icon: String
  },
  data() {
    return {
      isOpen: false
    };
  },
  methods: {
    toggleDropDown() {
      this.isOpen = !this.isOpen;
    },
    closeDropDown() {
      this.isOpen = false;
    }
  }
});

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    type: {
      type: String,
      default: 'text'
    },
    label: String,
    name: String,
    disabled: Boolean,
    placeholder: String,
    value: [String, Number]
  }
});

/***/ }),
/* 44 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'notification',
  props: {
    message: String,
    icon: String,
    verticalAlign: {
      type: String,
      default: 'top'
    },
    horizontalAlign: {
      type: String,
      default: 'center'
    },
    type: {
      type: String,
      default: 'info'
    },
    timeout: {
      type: Number,
      default: 5000
    }
  },
  data() {
    return {};
  },
  computed: {
    hasIcon() {
      return this.icon && this.icon.length > 0;
    },
    alertType() {
      return `alert-${this.type}`;
    },
    customPosition() {
      let initialMargin = 20;
      let alertHeight = 90;
      let sameAlertsCount = this.$notifications.state.filter(alert => {
        return alert.horizontalAlign === this.horizontalAlign && alert.verticalAlign === this.verticalAlign;
      }).length;
      let pixels = (sameAlertsCount - 1) * alertHeight + initialMargin;
      let styles = {};
      if (this.verticalAlign === 'top') {
        styles.top = `${pixels}px`;
      } else {
        styles.bottom = `${pixels}px`;
      }
      return styles;
    }
  },
  methods: {
    close() {
      this.$emit('on-close');
    }
  },
  mounted() {
    if (this.timeout) {
      setTimeout(this.close, this.timeout);
    }
  }
});

/***/ }),
/* 45 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Notification_vue__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Notification_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Notification_vue__);
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Notification: __WEBPACK_IMPORTED_MODULE_0__Notification_vue___default.a
  },
  data() {
    return {
      notifications: this.$notifications.state
    };
  },
  methods: {
    removeNotification(index) {
      this.$notifications.removeNotification(index);
    }
  }
});

/***/ }),
/* 46 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    columns: Array,
    data: Array,
    type: {
      type: String, // striped | hover
      default: 'striped'
    },
    title: {
      type: String,
      default: ''
    },
    subTitle: {
      type: String,
      default: ''

    }
  },
  computed: {
    tableClass() {
      return `table-${this.type}`;
    }
  },
  methods: {
    hasValue(item, column) {
      return item[column.toLowerCase()] !== 'undefined';
    },
    itemValue(item, column) {
      return item[column.toLowerCase()];
    }
  }
});

/***/ }),
/* 47 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    moveY: {
      type: Number,
      default: 0
    }
  },
  computed: {
    /**
     * Styles to animate the arrow
     * @returns {{transform: string}}
     */
    arrowStyle() {
      return {
        transform: `translate3d(0px, ${this.moveY}px, 0px)`
      };
    }
  }
});

/***/ }),
/* 48 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__MovingArrow_vue__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__MovingArrow_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__MovingArrow_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    type: {
      type: String,
      default: 'sidebar',
      validator: value => {
        let acceptedValues = ['sidebar', 'navbar'];
        return acceptedValues.indexOf(value) !== -1;
      }
    },
    backgroundColor: {
      type: String,
      default: 'black',
      validator: value => {
        let acceptedValues = ['white', 'black', 'darkblue'];
        return acceptedValues.indexOf(value) !== -1;
      }
    },
    activeColor: {
      type: String,
      default: 'success',
      validator: value => {
        let acceptedValues = ['primary', 'info', 'success', 'warning', 'danger'];
        return acceptedValues.indexOf(value) !== -1;
      }
    },
    sidebarLinks: {
      type: Array,
      default: () => []
    }
  },
  components: {
    MovingArrow: __WEBPACK_IMPORTED_MODULE_0__MovingArrow_vue___default.a
  },
  computed: {
    sidebarClasses() {
      if (this.type === 'sidebar') {
        return 'sidebar';
      } else {
        return 'collapse navbar-collapse off-canvas-sidebar';
      }
    },
    navClasses() {
      if (this.type === 'sidebar') {
        return 'nav';
      } else {
        return 'nav navbar-nav';
      }
    },
    /**
     * Styles to animate the arrow near the current active sidebar link
     * @returns {{transform: string}}
     */
    arrowMovePx() {
      return this.linkHeight * this.activeLinkIndex;
    }
  },
  data() {
    return {
      linkHeight: 60,
      activeLinkIndex: 0,

      windowWidth: 0,
      isWindows: false,
      hasAutoHeight: false
    };
  },
  methods: {
    findActiveLink() {
      this.sidebarLinks.find((element, index) => {
        let found = element.path === this.$route.path;
        if (found) {
          this.activeLinkIndex = index;
        }
        return found;
      });
    }
  },
  mounted() {
    this.findActiveLink();
  },
  watch: {
    $route: function (newRoute, oldRoute) {
      this.findActiveLink();
    }
  }
});

/***/ }),
/* 49 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_router__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_resource__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__globalComponents__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__globalDirectives__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_UIComponents_NotificationPlugin__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_UIComponents_SidebarPlugin__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__App__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__App___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__App__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__routes_routes__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_chartist__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_chartist___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_chartist__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_bootstrap_dist_css_bootstrap_css__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_bootstrap_dist_css_bootstrap_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_bootstrap_dist_css_bootstrap_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_sass_paper_dashboard_scss__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_sass_paper_dashboard_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__assets_sass_paper_dashboard_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_es6_promise_auto__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_es6_promise_auto___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_es6_promise_auto__);



// Plugins






// router setup


// library imports





// plugin setup
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vue_router__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_3__globalComponents__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_4__globalDirectives__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_5__components_UIComponents_NotificationPlugin__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_6__components_UIComponents_SidebarPlugin__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_2_vue_resource__["a" /* default */]);

// configure router
const router = new __WEBPACK_IMPORTED_MODULE_1_vue_router__["a" /* default */]({
  routes: __WEBPACK_IMPORTED_MODULE_8__routes_routes__["a" /* default */], // short for routes: routes
  linkActiveClass: 'active'
});

// global library setup
Object.defineProperty(__WEBPACK_IMPORTED_MODULE_0_vue___default.a.prototype, '$Chartist', {
  get() {
    return this.$root.Chartist;
  }
});

/* eslint-disable no-new */
new __WEBPACK_IMPORTED_MODULE_0_vue___default.a({
  el: '#app',
  render: h => h(__WEBPACK_IMPORTED_MODULE_7__App___default.a),
  router,
  data: {
    Chartist: __WEBPACK_IMPORTED_MODULE_9_chartist___default.a
  }
});

/***/ }),
/* 50 */,
/* 51 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 52 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 53 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 54 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 55 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 56 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 57 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 58 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 59 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 60 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 61 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 62 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 63 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 64 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 65 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 66 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 67 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 68 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 69 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 70 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 71 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 72 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 73 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 74 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 75 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 76 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 77 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 78 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 79 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 80 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 81 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 82 */,
/* 83 */,
/* 84 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(67)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(17),
  /* template */
  __webpack_require__(132),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(70)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(18),
  /* template */
  __webpack_require__(135),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(57)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(19),
  /* template */
  __webpack_require__(121),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(81)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(20),
  /* template */
  __webpack_require__(147),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(71)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(21),
  /* template */
  __webpack_require__(136),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(54)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(22),
  /* template */
  __webpack_require__(118),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(72)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(23),
  /* template */
  __webpack_require__(137),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(62)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(24),
  /* template */
  __webpack_require__(127),
  /* scopeId */
  "data-v-3f9112ee",
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(65)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(25),
  /* template */
  __webpack_require__(130),
  /* scopeId */
  "data-v-4d6b81ea",
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(77)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(26),
  /* template */
  __webpack_require__(143),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(53)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(27),
  /* template */
  __webpack_require__(117),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(79)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(28),
  /* template */
  __webpack_require__(145),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(74)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(29),
  /* template */
  __webpack_require__(139),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(75)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(30),
  /* template */
  __webpack_require__(140),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(63)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(31),
  /* template */
  __webpack_require__(128),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(66)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(32),
  /* template */
  __webpack_require__(131),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(58)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(33),
  /* template */
  __webpack_require__(123),
  /* scopeId */
  "data-v-25a54bd9",
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(60)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(34),
  /* template */
  __webpack_require__(125),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(68)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(35),
  /* template */
  __webpack_require__(133),
  /* scopeId */
  "data-v-60331cb2",
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(69)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(36),
  /* template */
  __webpack_require__(134),
  /* scopeId */
  "data-v-606d3df1",
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(56)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(37),
  /* template */
  __webpack_require__(120),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(73)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(38),
  /* template */
  __webpack_require__(138),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(39),
  /* template */
  __webpack_require__(141),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(51)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(40),
  /* template */
  __webpack_require__(115),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(76)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(41),
  /* template */
  __webpack_require__(142),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(42),
  /* template */
  __webpack_require__(122),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(52)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(43),
  /* template */
  __webpack_require__(116),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(78)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(45),
  /* template */
  __webpack_require__(144),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(61)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(46),
  /* template */
  __webpack_require__(126),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(80)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(47),
  /* template */
  __webpack_require__(146),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(55)

var Component = __webpack_require__(0)(
  /* script */
  __webpack_require__(48),
  /* template */
  __webpack_require__(119),
  /* scopeId */
  null,
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),
/* 115 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "header"
  }, [_vm._t("title"), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._t("subTitle")], 2)], 2), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "ct-chart",
    attrs: {
      "id": _vm.chartId
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "footer"
  }, [_c('div', {
    staticClass: "chart-legend"
  }, [_vm._t("legend")], 2), _vm._v(" "), _c('hr'), _vm._v(" "), _c('div', {
    staticClass: "stats"
  }, [_vm._t("footer")], 2), _vm._v(" "), _c('div', {
    staticClass: "pull-right"
  })])])])
},staticRenderFns: []}

/***/ }),
/* 116 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "form-group"
  }, [(_vm.label) ? _c('label', [_vm._v("\n    " + _vm._s(_vm.label) + "\n  ")]) : _vm._e(), _vm._v(" "), _c('input', _vm._b({
    staticClass: "form-control border-input",
    domProps: {
      "value": _vm.value
    },
    on: {
      "input": function($event) {
        _vm.$emit('input', $event.target.value)
      }
    }
  }, 'input', _vm.$props, false))])
},staticRenderFns: []}

/***/ }),
/* 117 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card card-map"
  }, [_c('div', {
    staticClass: "header"
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("Google Maps")])]), _vm._v(" "), _c('div', {
    staticClass: "map"
  }, [_c('div', {
    attrs: {
      "id": "map"
    }
  })])])
}]}

/***/ }),
/* 118 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('h1', [_vm._v("Add New Sculpture")]), _vm._v(" "), (_vm.imgURL != 'NONE') ? _c('img', {
    staticClass: "img-responsive",
    attrs: {
      "height": "700px",
      "width": "300px",
      "src": _vm.imgURL,
      "alt": ""
    }
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Name",
      "placeholder": "Name of the photo"
    },
    model: {
      value: (_vm.painting.name),
      callback: function($$v) {
        _vm.painting.name = $$v
      },
      expression: "painting.name"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "textarea",
      "label": "Description",
      "placeholder": "Description"
    },
    model: {
      value: (_vm.painting.description),
      callback: function($$v) {
        _vm.painting.description = $$v
      },
      expression: "painting.description"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "URL for Image",
      "placeholder": "URL for Image"
    },
    model: {
      value: (_vm.painting.photo),
      callback: function($$v) {
        _vm.painting.photo = $$v
      },
      expression: "painting.photo"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row",
    staticStyle: {
      "margin-top": "20px"
    }
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('input', {
    staticClass: "form-control",
    attrs: {
      "type": "file",
      "accept": "image/*"
    },
    on: {
      "change": function($event) {
        _vm.upload($event.target.files)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "text-center"
  }, [_c('button', {
    staticClass: "btn btn-info btn-fill btn-wd",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.uploadPainting($event)
      }
    }
  }, [_vm._v("\n            Upload\n        ")])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])
},staticRenderFns: []}

/***/ }),
/* 119 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    class: _vm.sidebarClasses,
    attrs: {
      "data-background-color": _vm.backgroundColor,
      "data-active-color": _vm.activeColor
    }
  }, [_c('div', {
    staticClass: "sidebar-wrapper",
    attrs: {
      "id": "style-3"
    }
  }, [_vm._m(0), _vm._v(" "), _vm._t("default"), _vm._v(" "), _c('ul', {
    class: _vm.navClasses
  }, _vm._l((_vm.sidebarLinks), function(link, index) {
    return _c('router-link', {
      ref: link.name,
      refInFor: true,
      attrs: {
        "to": link.path,
        "tag": "li"
      }
    }, [_c('a', [_c('i', {
      class: link.icon
    }), _vm._v(" "), _c('p', [_vm._v(_vm._s(link.name) + "\n          ")])])])
  })), _vm._v(" "), _c('moving-arrow', {
    attrs: {
      "move-y": _vm.arrowMovePx
    }
  })], 2)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "logo"
  }, [_c('a', {
    staticClass: "simple-text",
    attrs: {
      "href": "#"
    }
  }, [_vm._v("\n        Art Gallery\n      ")])])
}]}

/***/ }),
/* 120 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "header"
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v(_vm._s(_vm.title))])]), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_c('ul', {
    staticClass: "list-unstyled team-members"
  }, [_c('li', _vm._l((_vm.members), function(member) {
    return _c('div', {
      staticClass: "row"
    }, [_c('div', {
      staticClass: "col-xs-3"
    }, [_c('div', {
      staticClass: "avatar"
    }, [_c('img', {
      staticClass: "img-circle img-no-padding img-responsive",
      attrs: {
        "src": member.image,
        "alt": "Circle Image"
      }
    })])]), _vm._v(" "), _c('div', {
      staticClass: "col-xs-6"
    }, [_vm._v("\n            " + _vm._s(member.name) + "\n            "), _c('br'), _vm._v(" "), _c('span', {
      class: _vm.getStatusClass(member.status)
    }, [_c('small', [_vm._v(_vm._s(member.status))])])]), _vm._v(" "), _vm._m(0, true)])
  }))])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-xs-3 text-right"
  }, [_c('button', {
    staticClass: "btn btn-sm btn-success btn-icon"
  }, [_c('i', {
    staticClass: "fa fa-envelope"
  })])])
}]}

/***/ }),
/* 121 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrapper"
  }, [(_vm.loggedIn == 0) ? _c('side-bar', {
    attrs: {
      "type": "sidebar",
      "sidebar-links": _vm.$sidebar.userLinks
    }
  }) : _vm._e(), _vm._v(" "), (_vm.loggedIn == 1) ? _c('side-bar', {
    attrs: {
      "type": "sidebar",
      "sidebar-links": _vm.$sidebar.adminLinks
    }
  }) : _vm._e(), _vm._v(" "), (_vm.loggedIn == -1) ? _c('side-bar', {
    attrs: {
      "type": "sidebar",
      "sidebar-links": _vm.$sidebar.guestLinks
    }
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "main-panel"
  }, [_c('top-navbar'), _vm._v(" "), _c('dashboard-content', {
    nativeOn: {
      "click": function($event) {
        _vm.toggleSidebar($event)
      }
    }
  }), _vm._v(" "), _c('content-footer')], 1)], 1)
},staticRenderFns: []}

/***/ }),
/* 122 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', {
    directives: [{
      name: "click-outside",
      rawName: "v-click-outside",
      value: (_vm.closeDropDown),
      expression: "closeDropDown"
    }],
    staticClass: "dropdown",
    class: {
      open: _vm.isOpen
    },
    on: {
      "click": _vm.toggleDropDown
    }
  }, [_c('a', {
    staticClass: "dropdown-toggle btn-rotate",
    attrs: {
      "data-toggle": "dropdown",
      "href": "javascript:void(0)"
    }
  }, [_vm._t("title", [_c('i', {
    class: _vm.icon
  }), _vm._v(" "), _c('p', {
    staticClass: "notification"
  }, [_vm._v(_vm._s(_vm.title) + "\n        "), _c('b', {
    staticClass: "caret"
  })])])], 2), _vm._v(" "), _c('ul', {
    staticClass: "dropdown-menu"
  }, [_vm._t("default")], 2)])
},staticRenderFns: []}

/***/ }),
/* 123 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [(_vm.selected == 'Painting') ? _c('router-link', {
    staticClass: "btn",
    attrs: {
      "to": "/app/add_painting"
    }
  }, [_vm._v("Add Painting")]) : _vm._e(), _vm._v(" "), (_vm.selected == 'Sculpture') ? _c('router-link', {
    staticClass: "btn",
    attrs: {
      "to": "/app/add_sculpture"
    }
  }, [_vm._v("Add Sculpture")]) : _vm._e()], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('h1', {
    staticClass: "page-header"
  }, [_c('small', [_vm._v(_vm._s(_vm.selected) + " Gallery")])])])]), _vm._v(" "), _vm._l((Math.ceil(_vm.objs.length / 3)), function(m) {
    return (!_vm.editMode) ? _c('div', {
      staticClass: "row"
    }, _vm._l((3), function(n) {
      return (((m - 1) * 3 + (n - 1)) < _vm.objs.length) ? _c('div', {
        staticClass: "col-md-3 portfolio-item"
      }, [_c('span', [_c('img', {
        staticClass: "img-responsive",
        attrs: {
          "src": _vm.objs[(m - 1) * 3 + (n - 1)].photo,
          "alt": ""
        }
      })]), _vm._v(" "), _c('h3', [_c('span', [_c('strong', [_vm._v(_vm._s(_vm.objs[(m - 1) * 3 + (n - 1)].name))])])]), _vm._v(" "), _c('p', [_vm._v("\n          " + _vm._s(_vm.objs[(m - 1) * 3 + (n - 1)].description) + "\n        ")]), _vm._v(" "), _c('div', {
        staticClass: "btn sm",
        on: {
          "click": function($event) {
            _vm.deletePhoto(_vm.objs[(m - 1) * 3 + (n - 1)].id)
          }
        }
      }, [_vm._v("Delete")]), _vm._v(" "), _c('div', {
        staticClass: "btn sm",
        on: {
          "click": function($event) {
            _vm.editPhoto(_vm.objs[(m - 1) * 3 + (n - 1)])
          }
        }
      }, [_vm._v("Edit")])]) : _vm._e()
    })) : _vm._e()
  }), _vm._v(" "), (_vm.objs.length < 1) ? _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('h1', [_vm._v("No " + _vm._s(_vm.selected) + " Found")])])]) : _vm._e(), _vm._v(" "), (_vm.editMode) ? _c('div', {
    staticClass: "row"
  }, [_c('h3', [_vm._v("Edit")]), _vm._v(" "), _c('img', {
    staticClass: "img-responsive",
    attrs: {
      "height": "700px",
      "width": "300px",
      "src": _vm.painting.photo,
      "alt": ""
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Name",
      "placeholder": "Name of the photo"
    },
    model: {
      value: (_vm.painting.name),
      callback: function($$v) {
        _vm.painting.name = $$v
      },
      expression: "painting.name"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('fg-input', {
    attrs: {
      "type": "textarea",
      "label": "Description",
      "placeholder": "Description"
    },
    model: {
      value: (_vm.painting.description),
      callback: function($$v) {
        _vm.painting.description = $$v
      },
      expression: "painting.description"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "text-center"
  }, [_c('button', {
    staticClass: "btn btn-info btn-fill btn-wd",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.edit($event)
      }
    }
  }, [_vm._v("\n          Update\n        ")])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })]) : _vm._e()], 2)
},staticRenderFns: []}

/***/ }),
/* 124 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    class: {
      'nav-open': _vm.$sidebar.showSidebar
    }
  }, [_c('router-view'), _vm._v(" "), _c('side-bar', {
    attrs: {
      "type": "navbar",
      "sidebar-links": _vm.$sidebar.sidebarLinks
    }
  }, [_c('ul', {
    staticClass: "nav navbar-nav"
  }, [_c('li', [_c('a', {
    staticClass: "dropdown-toggle",
    attrs: {
      "data-toggle": "dropdown"
    }
  }, [_c('i', {
    staticClass: "ti-panel"
  }), _vm._v(" "), _c('p', [_vm._v("Stats")])])]), _vm._v(" "), _c('drop-down', {
    attrs: {
      "title": "5 Notifications",
      "icon": "ti-bell"
    }
  }, [_c('li', [_c('a', [_vm._v("Notification 1")])]), _vm._v(" "), _c('li', [_c('a', [_vm._v("Notification 2")])]), _vm._v(" "), _c('li', [_c('a', [_vm._v("Notification 3")])]), _vm._v(" "), _c('li', [_c('a', [_vm._v("Notification 4")])]), _vm._v(" "), _c('li', [_c('a', [_vm._v("Another notification")])])]), _vm._v(" "), _c('li', [_c('a', [_c('i', {
    staticClass: "ti-settings"
  }), _vm._v(" "), _c('p', [_vm._v("Settings")])])]), _vm._v(" "), _c('li', {
    staticClass: "divider"
  })], 1)])], 1)
},staticRenderFns: []}

/***/ }),
/* 125 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-4 col-md-5"
  }, [_c('user-card')], 1), _vm._v(" "), _c('div', {
    staticClass: "col-lg-8 col-md-7"
  }, [_c('edit-profile-form')], 1), _vm._v(" "), _c('List')], 1)
},staticRenderFns: []}

/***/ }),
/* 126 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "header"
  }, [_vm._t("header", [_c('h4', {
    staticClass: "title"
  }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v(_vm._s(_vm.subTitle))])])], 2), _vm._v(" "), _c('div', {
    staticClass: "content table-responsive table-full-width"
  }, [_c('table', {
    staticClass: "table",
    class: _vm.tableClass
  }, [_c('thead', _vm._l((_vm.columns), function(column) {
    return _c('th', [_vm._v(_vm._s(column))])
  })), _vm._v(" "), _c('tbody', _vm._l((_vm.data), function(item) {
    return _c('tr', _vm._l((_vm.columns), function(column) {
      return (_vm.hasValue(item, column)) ? _c('td', [_vm._v(_vm._s(_vm.itemValue(item, column)))]) : _vm._e()
    }))
  }))])])])
},staticRenderFns: []}

/***/ }),
/* 127 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('h1', {
    staticClass: "page-header"
  }, [_c('small', [_vm._v(_vm._s(_vm.selected) + " Gallery")])])])]), _vm._v(" "), _vm._l((Math.ceil(_vm.objs.length / 3)), function(m) {
    return _c('div', {
      staticClass: "row"
    }, _vm._l((3), function(n) {
      return (((m - 1) * 3 + (n - 1)) < _vm.objs.length) ? _c('div', {
        staticClass: "col-md-3 portfolio-item"
      }, [_c('span', [_c('img', {
        staticClass: "img-responsive",
        attrs: {
          "src": _vm.objs[(m - 1) * 3 + (n - 1)].photo,
          "alt": ""
        }
      })]), _vm._v(" "), _c('h3', [_c('span', [_c('strong', [_vm._v(_vm._s(_vm.objs[(m - 1) * 3 + (n - 1)].name))]), _c('i', [_vm._v("  by  "), _c('router-link', {
        attrs: {
          "to": '/app/profile/' + _vm.objs[(m - 1) * 3 + (n - 1)].artist_id
        }
      }, [_vm._v(_vm._s(_vm.selected === 'Painting' ? _vm.objs[(m - 1) * 3 + (n - 1)].painter.name : _vm.objs[(m - 1) * 3 + (n - 1)].sculpted.name))])], 1)])]), _vm._v(" "), _c('p', [_vm._v("\n        " + _vm._s(_vm.objs[(m - 1) * 3 + (n - 1)].description) + "\n      ")]), _vm._v(" "), (_vm.isAdmin) ? _c('div', {
        staticClass: "btn sm",
        on: {
          "click": function($event) {
            _vm.deletePhoto(_vm.objs[(m - 1) * 3 + (n - 1)].id)
          }
        }
      }, [_vm._v("Delete")]) : _vm._e()]) : _vm._e()
    }))
  }), _vm._v(" "), (_vm.objs.length < 1) ? _c('div', {
    staticClass: "row"
  }, [_c('h1', [_vm._v("NO " + _vm._s(_vm.selected) + " FOUND")])]) : _vm._e(), _vm._v(" "), _c('hr'), _vm._v(" "), _c('div', {
    staticClass: "row text-center"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('ul', {
    staticClass: "pagination"
  }, [_vm._m(0), _vm._v(" "), _vm._l((_vm.noPage), function(n) {
    return _c('li', {
      class: {
        active: n == _vm.currentPage
      }
    }, [_c('router-link', {
      attrs: {
        "to": '' + n
      }
    }, [_vm._v(_vm._s(n))])], 1)
  }), _vm._v(" "), _vm._m(1)], 2)])])], 2)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', [_vm._v("«")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', [_vm._v("»")])])
}]}

/***/ }),
/* 128 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "card"
  }, [_c('paper-table', {
    attrs: {
      "title": _vm.table1.title,
      "sub-title": _vm.table1.subTitle,
      "data": _vm.table1.data,
      "columns": _vm.table1.columns
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "card card-plain"
  }, [_c('paper-table', {
    attrs: {
      "type": "hover",
      "title": _vm.table2.title,
      "sub-title": _vm.table2.subTitle,
      "data": _vm.table2.data,
      "columns": _vm.table2.columns
    }
  })], 1)])])
},staticRenderFns: []}

/***/ }),
/* 129 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-xs-11 col-sm-4 alert open alert-with-icon",
    class: [_vm.verticalAlign, _vm.horizontalAlign, _vm.alertType],
    style: (_vm.customPosition),
    attrs: {
      "data-notify": "container",
      "role": "alert",
      "data-notify-position": "top-center"
    }
  }, [_c('button', {
    staticClass: "close col-xs-1",
    attrs: {
      "type": "button",
      "aria-hidden": "true",
      "data-notify": "dismiss"
    },
    on: {
      "click": _vm.close
    }
  }, [_vm._v("×\n  ")]), _vm._v(" "), _c('span', {
    staticClass: "alert-icon",
    class: _vm.icon,
    attrs: {
      "data-notify": "icon"
    }
  }), _vm._v(" "), _c('span', {
    attrs: {
      "data-notify": "message"
    },
    domProps: {
      "innerHTML": _vm._s(_vm.message)
    }
  })])
},staticRenderFns: []}

/***/ }),
/* 130 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [_vm._m(0), _vm._v(" "), _vm._l((Math.ceil(_vm.objs.length / 3)), function(m) {
    return _c('div', {
      staticClass: "row"
    }, _vm._l((3), function(n) {
      return (((m - 1) * 3 + (n - 1)) < _vm.objs.length) ? _c('div', {
        staticClass: "col-md-3 portfolio-item"
      }, [_c('span', [_c('img', {
        staticClass: "img-responsive",
        attrs: {
          "src": _vm.objs[(m - 1) * 3 + (n - 1)].profile_photo,
          "alt": ""
        }
      })]), _vm._v(" "), _c('h3', [_c('span', [_c('i', [_c('router-link', {
        attrs: {
          "to": '/app/profile/' + _vm.objs[(m - 1) * 3 + (n - 1)].id
        }
      }, [_vm._v(_vm._s(_vm.objs[(m - 1) * 3 + (n - 1)].name))])], 1)])]), _vm._v(" "), _c('p', [_vm._v("\n        Email: " + _vm._s(_vm.objs[(m - 1) * 3 + (n - 1)].email) + " "), _c('br'), _vm._v("\n        Phone: " + _vm._s(_vm.objs[(m - 1) * 3 + (n - 1)].phone) + " "), _c('br')]), _vm._v(" "), (_vm.isAdmin) ? _c('div', {
        staticClass: "btn sm",
        on: {
          "click": function($event) {
            _vm.deletePhoto(_vm.objs[(m - 1) * 3 + (n - 1)].id)
          }
        }
      }, [_vm._v("Delete")]) : _vm._e()]) : _vm._e()
    }))
  }), _vm._v(" "), (_vm.objs.length < 1) ? _c('div', {
    staticClass: "row"
  }, [_c('h3', [_vm._v("No users found")])]) : _vm._e(), _vm._v(" "), _c('hr'), _vm._v(" "), _c('div', {
    staticClass: "row text-center"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('ul', {
    staticClass: "pagination"
  }, [_vm._m(1), _vm._v(" "), _vm._l((_vm.noPage), function(n) {
    return _c('li', {
      class: {
        active: n == _vm.currentPage
      }
    }, [_c('router-link', {
      attrs: {
        "to": '' + n
      }
    }, [_vm._v(_vm._s(n))])], 1)
  }), _vm._v(" "), _vm._m(2)], 2)])])], 2)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('h1', {
    staticClass: "page-header"
  }, [_c('small', [_vm._v("User List")])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', [_vm._v("«")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('li', [_c('a', [_vm._v("»")])])
}]}

/***/ }),
/* 131 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "header"
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("Paper Dashboard Headings")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Created using\n         "), _c('a', {
    attrs: {
      "href": "https://www.google.com/fonts/specimen/Muli"
    }
  }, [_vm._v("Muli")]), _vm._v(" Font Family")])]), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "typo-line"
  }, [_c('h1', [_c('p', {
    staticClass: "category"
  }, [_vm._v("Header 1")]), _vm._v("Paper Dashboard Heading ")])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('h2', [_c('p', {
    staticClass: "category"
  }, [_vm._v("Header 2")]), _vm._v("Paper Dashboard Heading ")])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('h3', [_c('p', {
    staticClass: "category"
  }, [_vm._v("Header 3")]), _vm._v("Paper Dashboard Heading ")])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('h4', [_c('p', {
    staticClass: "category"
  }, [_vm._v("Header 4")]), _vm._v("Paper Dashboard Heading ")])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('h5', [_c('p', {
    staticClass: "category"
  }, [_vm._v("Header 5")]), _vm._v("Paper Dashboard Heading ")])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('h6', [_c('p', {
    staticClass: "category"
  }, [_vm._v("Header 6")]), _vm._v("Paper Dashboard Heading ")])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('p', [_c('span', {
    staticClass: "category"
  }, [_vm._v("Paragraph")]), _vm._v("Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.\n         ")])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('p', {
    staticClass: "category"
  }, [_vm._v("Quote")]), _vm._v(" "), _c('blockquote', [_c('p', [_vm._v("\n             Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.\n           ")]), _vm._v(" "), _c('small', [_vm._v("\n             Steve Jobs, CEO Apple\n           ")])])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('p', {
    staticClass: "category"
  }, [_vm._v("Muted Text")]), _vm._v(" "), _c('p', {
    staticClass: "text-muted"
  }, [_vm._v("\n           Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.\n         ")])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('p', {
    staticClass: "category"
  }, [_vm._v("Coloured Text")]), _vm._v(" "), _c('p', {
    staticClass: "text-primary"
  }, [_vm._v("\n           Text Primary - Light Bootstrap Table Heading and complex bootstrap dashboard you've ever seen on the internet.\n         ")]), _vm._v(" "), _c('p', {
    staticClass: "text-info"
  }, [_vm._v("\n           Text Info - Light Bootstrap Table Heading and complex bootstrap dashboard you've ever seen on the internet.\n         ")]), _vm._v(" "), _c('p', {
    staticClass: "text-success"
  }, [_vm._v("\n           Text Success - Light Bootstrap Table Heading and complex bootstrap dashboard you've ever seen on the internet.\n         ")]), _vm._v(" "), _c('p', {
    staticClass: "text-warning"
  }, [_vm._v("\n           Text Warning - Light Bootstrap Table Heading and complex bootstrap dashboard you've ever seen on the internet.\n         ")]), _vm._v(" "), _c('p', {
    staticClass: "text-danger"
  }, [_vm._v("\n           Text Danger - Light Bootstrap Table Heading and complex bootstrap dashboard you've ever seen on the internet.\n         ")])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('h2', [_c('p', {
    staticClass: "category"
  }, [_vm._v("Small Tag")]), _vm._v("Header with small subtitle\n           "), _c('br'), _vm._v(" "), _c('small', [_vm._v("\".small\" is a tag for the headers")])])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('p', {
    staticClass: "category"
  }, [_vm._v("Lists")]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-3"
  }, [_c('h5', [_vm._v("Unordered List")]), _vm._v(" "), _c('ul', [_c('li', [_vm._v("List Item")]), _vm._v(" "), _c('li', [_vm._v("List Item")]), _vm._v(" "), _c('li', {
    staticClass: "list-unstyled"
  }, [_c('ul', [_c('li', [_vm._v("List Item")]), _vm._v(" "), _c('li', [_vm._v("List Item")]), _vm._v(" "), _c('li', [_vm._v("List Item")])])]), _vm._v(" "), _c('li', [_vm._v("List Item")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('h5', [_vm._v("Ordered List")]), _vm._v(" "), _c('ol', [_c('li', [_vm._v("List Item")]), _vm._v(" "), _c('li', [_vm._v("List Item")]), _vm._v(" "), _c('li', [_vm._v("List Item")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('h5', [_vm._v("Unstyled List")]), _vm._v(" "), _c('ul', {
    staticClass: "list-unstyled"
  }, [_c('li', [_vm._v("List Item")]), _vm._v(" "), _c('li', [_vm._v("List Item")]), _vm._v(" "), _c('li', [_vm._v("List Item")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('h5', [_vm._v("Inline List")]), _vm._v(" "), _c('ul', {
    staticClass: "list-inline"
  }, [_c('li', [_vm._v("List Item")]), _vm._v(" "), _c('li', [_vm._v("List Item")]), _vm._v(" "), _c('li', [_vm._v("List Item")])])])])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('p', {
    staticClass: "category"
  }, [_vm._v("Blockquotes")]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('h5', [_vm._v("Default Blockquote")]), _vm._v(" "), _c('blockquote', [_c('p', [_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('h5', [_vm._v("Blockquote with Citation")]), _vm._v(" "), _c('blockquote', [_c('p', [_vm._v("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.")]), _vm._v(" "), _c('small', [_vm._v("Someone famous in\n                 "), _c('cite', {
    attrs: {
      "title": "Source Title"
    }
  }, [_vm._v("Source Title")])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "typo-line"
  }, [_c('p', {
    staticClass: "category"
  }, [_vm._v("Code")]), _vm._v(" "), _c('p', [_vm._v("\n           This is\n           "), _c('code', [_vm._v(".css-class-as-code")]), _vm._v(", an example of an inline code element. Wrap inline code within a\n           "), _c('code', [_vm._v("\n             <code>...</code>")]), _vm._v("tag.")]), _vm._v(" "), _c('pre', [_vm._v("1. #This is an example of preformatted text. 2. #Here is another line of code")])])])])])])
}]}

/***/ }),
/* 132 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_c('transition', {
    attrs: {
      "name": "fade",
      "mode": "out-in"
    }
  }, [_c('router-view')], 1)], 1)])
},staticRenderFns: []}

/***/ }),
/* 133 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card"
  }, [_vm._m(0), _vm._v(" "), (!_vm.pub) ? _c('div', {
    staticClass: "content"
  }, [_c('form', [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "email",
      "label": "Login Username/Email",
      "placeholder": "Email"
    },
    model: {
      value: (_vm.user.email),
      callback: function($$v) {
        _vm.user.email = $$v
      },
      expression: "user.email"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Name",
      "placeholder": "Full Name"
    },
    model: {
      value: (_vm.user.name),
      callback: function($$v) {
        _vm.user.name = $$v
      },
      expression: "user.name"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Phone",
      "placeholder": "Phone"
    },
    model: {
      value: (_vm.user.phone),
      callback: function($$v) {
        _vm.user.phone = $$v
      },
      expression: "user.phone"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Profile Picture Link",
      "placeholder": "Profile Picture Link"
    },
    model: {
      value: (_vm.user.profile_photo),
      callback: function($$v) {
        _vm.user.profile_photo = $$v
      },
      expression: "user.profile_photo"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "text-center"
  }, [(!_vm.pub) ? _c('button', {
    staticClass: "btn btn-info btn-fill btn-wd",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.updateProfile($event)
      }
    }
  }, [_vm._v("\n          Update Profile\n        ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])]) : _vm._e(), _vm._v(" "), (_vm.pub) ? _c('div', {
    staticClass: "content"
  }, [_c('form', [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-', {
    attrs: {
      "type": "text",
      "label": "Email",
      "placeholder": "Email"
    },
    model: {
      value: (_vm.user.email),
      callback: function($$v) {
        _vm.user.email = $$v
      },
      expression: "user.email"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Email",
      "disabled": ""
    },
    model: {
      value: (_vm.user.email),
      callback: function($$v) {
        _vm.user.email = $$v
      },
      expression: "user.email"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Name",
      "placeholder": "Full Name",
      "disabled": ""
    },
    model: {
      value: (_vm.user.name),
      callback: function($$v) {
        _vm.user.name = $$v
      },
      expression: "user.name"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Phone",
      "placeholder": "Phone",
      "disabled": ""
    },
    model: {
      value: (_vm.user.phone),
      callback: function($$v) {
        _vm.user.phone = $$v
      },
      expression: "user.phone"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Profile Picture Link",
      "placeholder": "Profile Picture Link",
      "disabled": ""
    },
    model: {
      value: (_vm.user.profile_photo),
      callback: function($$v) {
        _vm.user.profile_photo = $$v
      },
      expression: "user.profile_photo"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "text-center"
  }, [(!_vm.pub) ? _c('button', {
    staticClass: "btn btn-info btn-fill btn-wd",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.updateProfile($event)
      }
    }
  }, [_vm._v("\n          Update Profile\n        ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "header"
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("Edit Profile")])])
}]}

/***/ }),
/* 134 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.publ) ? _c('div', {
    staticClass: "container border-0 rounded"
  }, [_vm._m(0), _vm._v(" "), _vm._l((Math.ceil(_vm.paints.length / 3)), function(m) {
    return _c('div', {
      staticClass: "row"
    }, _vm._l((3), function(n) {
      return (((m - 1) * 3 + (n - 1)) < _vm.paints.length) ? _c('div', {
        staticClass: "col-md-3 portfolio-item"
      }, [_c('span', [_c('img', {
        staticClass: "img-responsive",
        attrs: {
          "src": _vm.paints[(m - 1) * 3 + (n - 1)].photo,
          "alt": ""
        }
      })]), _vm._v(" "), _c('h3', [_c('span', [_c('strong', [_vm._v(_vm._s(_vm.paints[(m - 1) * 3 + (n - 1)].name))])])]), _vm._v(" "), _c('p', [_vm._v("\n          " + _vm._s(_vm.paints[(m - 1) * 3 + (n - 1)].description) + "\n        ")])]) : _vm._e()
    }))
  }), _vm._v(" "), (_vm.paints.length < 1) ? _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-2 col-md-offset-5"
  }, [_vm._v("No Paintings for this User")])]) : _vm._e(), _vm._v(" "), _vm._m(1), _vm._v(" "), _vm._l((Math.ceil(_vm.sculps.length / 3)), function(m) {
    return _c('div', {
      staticClass: "row"
    }, _vm._l((3), function(n) {
      return (((m - 1) * 3 + (n - 1)) < _vm.sculps.length) ? _c('div', {
        staticClass: "col-md-3 portfolio-item"
      }, [_c('span', [_c('img', {
        staticClass: "img-responsive",
        attrs: {
          "src": _vm.sculps[(m - 1) * 3 + (n - 1)].photo,
          "alt": ""
        }
      })]), _vm._v(" "), _c('h3', [_c('span', [_c('strong', [_vm._v(_vm._s(_vm.sculps[(m - 1) * 3 + (n - 1)].name))])])]), _vm._v(" "), _c('p', [_vm._v("\n          " + _vm._s(_vm.sculps[(m - 1) * 3 + (n - 1)].description) + "\n        ")])]) : _vm._e()
    }))
  }), _vm._v(" "), (_vm.sculps.length < 1) ? _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-3 col-md-offset-5"
  }, [_vm._v("No Sculptures for this User")])]) : _vm._e(), _vm._v(" "), _c('hr')], 2) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('h1', {
    staticClass: "page-header"
  }, [_c('small', [_vm._v("Paintings")])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-lg-12"
  }, [_c('h1', {
    staticClass: "page-header"
  }, [_c('small', [_vm._v("Sculptures")])])])])
}]}

/***/ }),
/* 135 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('footer', {
    staticClass: "footer"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_c('nav', {
    staticClass: "pull-left"
  }, [_c('ul', [_c('li', [_c('router-link', {
    attrs: {
      "to": {
        path: '/admin'
      }
    }
  })], 1)])]), _vm._v(" "), _c('div', {
    staticClass: "copyright pull-right"
  }, [_vm._v("\n      Art Gallery (CSE370 Group Project)\n    ")])])])
},staticRenderFns: []}

/***/ }),
/* 136 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('h1', [_vm._v("Add New Painting")]), _vm._v(" "), (_vm.imgURL != 'NONE') ? _c('img', {
    staticClass: "img-responsive",
    attrs: {
      "height": "700px",
      "width": "300px",
      "src": _vm.imgURL,
      "alt": ""
    }
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Name",
      "placeholder": "Name of the photo"
    },
    model: {
      value: (_vm.painting.name),
      callback: function($$v) {
        _vm.painting.name = $$v
      },
      expression: "painting.name"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "textarea",
      "label": "Description",
      "placeholder": "Description"
    },
    model: {
      value: (_vm.painting.description),
      callback: function($$v) {
        _vm.painting.description = $$v
      },
      expression: "painting.description"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "URL for Image",
      "placeholder": "URL for Image"
    },
    model: {
      value: (_vm.painting.photo),
      callback: function($$v) {
        _vm.painting.photo = $$v
      },
      expression: "painting.photo"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('input', {
    staticClass: "form-control",
    attrs: {
      "type": "file",
      "accept": "image/*"
    },
    on: {
      "change": function($event) {
        _vm.upload($event.target.files)
      }
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "text-center",
    staticStyle: {
      "margin-top": "20px"
    }
  }, [_c('button', {
    staticClass: "btn btn-info btn-fill btn-wd",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.uploadPainting($event)
      }
    }
  }, [_vm._v("\n            Upload\n        ")])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])
},staticRenderFns: []}

/***/ }),
/* 137 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "header"
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("320+ Themify Icons")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Handcrafted by our friends from\n          "), _c('a', {
    attrs: {
      "target": "_blank",
      "href": "https://themify.me/"
    }
  }, [_vm._v("Themify")]), _vm._v(".")])]), _vm._v(" "), _c('div', {
    staticClass: "content all-icons"
  }, [_c('div', {
    staticClass: "icon-section"
  }, [_c('h3', [_vm._v("Arrows & Direction Icons ")]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrow-up"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrow-up")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrow-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrow-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrow-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrow-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrow-down"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrow-down")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrows-vertical"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrows-vertical")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrows-horizontal"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrows-horizontal")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-angle-up"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-angle-up")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-angle-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-angle-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-angle-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-angle-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-angle-down"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-angle-down")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-angle-double-up"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-angle-double-up")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-angle-double-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-angle-double-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-angle-double-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-angle-double-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-angle-double-down"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-angle-double-down")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-move"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-move")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-fullscreen"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-fullscreen")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrow-top-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrow-top-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrow-top-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrow-top-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrow-circle-up"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrow-circle-up")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrow-circle-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrow-circle-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrow-circle-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrow-circle-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrow-circle-down"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrow-circle-down")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-arrows-corner"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-arrows-corner")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-split-v"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-split-v")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-split-v-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-split-v-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-split-h"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-split-h")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-hand-point-up"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-hand-point-up")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-hand-point-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-hand-point-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-hand-point-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-hand-point-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-hand-point-down"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-hand-point-down")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-back-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-back-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-back-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-back-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-exchange-vertical"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-exchange-vertical")])])]), _vm._v(" "), _c('h3', [_vm._v("Web App Icons")]), _vm._v(" "), _c('div', {
    staticClass: "icon-section"
  }, [_c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-wand"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-wand")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-save"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-save")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-save-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-save-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-direction"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-direction")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-direction-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-direction-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-user"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-user")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-link"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-link")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-unlink"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-unlink")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-trash"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-trash")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-target"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-target")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-tag"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-tag")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-desktop"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-desktop")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-tablet"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-tablet")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-mobile"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-mobile")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-email"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-email")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-star"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-star")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-spray"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-spray")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-signal"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-signal")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-shopping-cart"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-shopping-cart")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-shopping-cart-full"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-shopping-cart-full")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-settings"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-settings")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-search"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-search")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-zoom-in"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-zoom-in")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-zoom-out"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-zoom-out")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-cut"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-cut")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-ruler"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-ruler")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-ruler-alt-2"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-ruler-alt-2")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-ruler-pencil"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-ruler-pencil")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-ruler-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-ruler-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-bookmark"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-bookmark")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-bookmark-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-bookmark-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-reload"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-reload")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-plus"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-plus")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-minus"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-minus")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-close"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-close")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-pin"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-pin")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-pencil"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-pencil")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-pencil-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-pencil-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-paint-roller"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-paint-roller")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-paint-bucket"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-paint-bucket")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-na"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-na")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-medall"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-medall")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-medall-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-medall-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-marker"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-marker")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-marker-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-marker-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-lock"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-lock")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-unlock"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-unlock")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-location-arrow"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-location-arrow")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layers"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layers")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layers-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layers-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-key"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-key")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-image"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-image")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-heart"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-heart")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-heart-broken"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-heart-broken")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-hand-stop"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-hand-stop")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-hand-open"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-hand-open")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-hand-drag"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-hand-drag")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-flag"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-flag")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-flag-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-flag-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-flag-alt-2"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-flag-alt-2")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-eye"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-eye")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-import"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-import")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-export"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-export")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-cup"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-cup")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-crown"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-crown")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-comments"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-comments")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-comment"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-comment")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-comment-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-comment-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-thought"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-thought")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-clip"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-clip")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-check"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-check")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-check-box"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-check-box")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-camera"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-camera")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-announcement"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-announcement")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-brush"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-brush")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-brush-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-brush-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-palette"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-palette")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-briefcase"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-briefcase")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-bolt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-bolt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-bolt-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-bolt-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-blackboard"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-blackboard")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-bag"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-bag")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-world"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-world")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-wheelchair"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-wheelchair")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-car"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-car")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-truck"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-truck")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-timer"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-timer")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-ticket"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-ticket")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-thumb-up"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-thumb-up")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-thumb-down"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-thumb-down")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-stats-up"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-stats-up")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-stats-down"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-stats-down")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-shine"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-shine")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-shift-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-shift-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-shift-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-shift-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-shift-right-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-shift-right-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-shift-left-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-shift-left-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-shield"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-shield")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-notepad"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-notepad")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-server"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-server")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-pulse"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-pulse")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-printer"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-printer")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-power-off"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-power-off")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-plug"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-plug")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-pie-chart"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-pie-chart")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-panel"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-panel")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-package"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-package")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-music"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-music")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-music-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-music-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-mouse"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-mouse")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-mouse-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-mouse-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-money"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-money")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-microphone"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-microphone")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-menu"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-menu")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-menu-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-menu-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-map"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-map")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-map-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-map-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-location-pin"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-location-pin")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-light-bulb"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-light-bulb")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-info"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-info")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-infinite"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-infinite")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-id-badge"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-id-badge")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-hummer"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-hummer")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-home"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-home")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-help"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-help")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-headphone"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-headphone")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-harddrives"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-harddrives")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-harddrive"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-harddrive")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-gift"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-gift")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-game"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-game")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-filter"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-filter")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-files"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-files")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-file"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-file")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-zip"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-zip")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-folder"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-folder")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-envelope"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-envelope")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-dashboard"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-dashboard")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-cloud"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-cloud")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-cloud-up"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-cloud-up")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-cloud-down"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-cloud-down")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-clipboard"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-clipboard")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-calendar"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-calendar")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-book"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-book")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-bell"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-bell")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-basketball"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-basketball")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-bar-chart"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-bar-chart")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-bar-chart-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-bar-chart-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-archive"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-archive")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-anchor"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-anchor")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-alert"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-alert")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-alarm-clock"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-alarm-clock")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-agenda"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-agenda")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-write"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-write")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-wallet"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-wallet")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-video-clapper"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-video-clapper")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-video-camera"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-video-camera")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-vector"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-vector")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-support"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-support")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-stamp"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-stamp")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-slice"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-slice")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-shortcode"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-shortcode")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-receipt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-receipt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-pin2"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-pin2")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-pin-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-pin-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-pencil-alt2"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-pencil-alt2")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-eraser"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-eraser")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-more"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-more")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-more-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-more-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-microphone-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-microphone-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-magnet"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-magnet")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-line-double"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-line-double")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-line-dotted"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-line-dotted")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-line-dashed"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-line-dashed")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-ink-pen"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-ink-pen")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-info-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-info-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-help-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-help-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-headphone-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-headphone-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-gallery"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-gallery")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-face-smile"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-face-smile")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-face-sad"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-face-sad")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-credit-card"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-credit-card")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-comments-smiley"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-comments-smiley")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-time"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-time")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-share"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-share")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-share-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-share-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-rocket"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-rocket")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-new-window"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-new-window")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-rss"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-rss")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-rss-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-rss-alt")])])]), _vm._v(" "), _c('div', {
    staticClass: "icon-section"
  }, [_c('h3', [_vm._v("Control Icons")]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-control-stop"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-control-stop")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-control-shuffle"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-control-shuffle")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-control-play"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-control-play")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-control-pause"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-control-pause")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-control-forward"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-control-forward")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-control-backward"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-control-backward")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-volume"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-volume")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-control-skip-forward"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-control-skip-forward")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-control-skip-backward"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-control-skip-backward")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-control-record"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-control-record")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-control-eject"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-control-eject")])])]), _vm._v(" "), _c('div', {
    staticClass: "icon-section"
  }, [_c('h3', [_vm._v("Text Editor")]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-paragraph"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-paragraph")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-uppercase"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-uppercase")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-underline"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-underline")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-text"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-text")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-Italic"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-Italic")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-smallcap"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-smallcap")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-list"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-list")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-list-ol"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-list-ol")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-align-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-align-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-align-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-align-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-align-justify"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-align-justify")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-align-center"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-align-center")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-quote-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-quote-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-quote-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-quote-left")])])]), _vm._v(" "), _c('div', {
    staticClass: "icon-section"
  }, [_c('h3', [_vm._v("Layout Icons")]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-width-full"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-width-full")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-width-default"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-width-default")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-width-default-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-width-default-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-tab"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-tab")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-tab-window"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-tab-window")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-tab-v"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-tab-v")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-tab-min"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-tab-min")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-slider"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-slider")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-slider-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-slider-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-sidebar-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-sidebar-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-sidebar-none"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-sidebar-none")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-sidebar-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-sidebar-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-placeholder"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-placeholder")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-menu"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-menu")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-menu-v"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-menu-v")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-menu-separated"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-menu-separated")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-menu-full"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-menu-full")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-media-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-media-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-media-right-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-media-right-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-media-overlay"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-media-overlay")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-media-overlay-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-media-overlay-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-media-overlay-alt-2"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-media-overlay-alt-2")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-media-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-media-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-media-left-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-media-left-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-media-center"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-media-center")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-media-center-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-media-center-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-list-thumb"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-list-thumb")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-list-thumb-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-list-thumb-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-list-post"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-list-post")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-list-large-image"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-list-large-image")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-line-solid"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-line-solid")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-grid4"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-grid4")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-grid3"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-grid3")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-grid2"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-grid2")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-grid2-thumb"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-grid2-thumb")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-cta-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-cta-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-cta-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-cta-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-cta-center"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-cta-center")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-cta-btn-right"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-cta-btn-right")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-cta-btn-left"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-cta-btn-left")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-column4"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-column4")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-column3"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-column3")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-column2"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-column2")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-accordion-separated"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-accordion-separated")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-accordion-merged"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-accordion-merged")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-accordion-list"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-accordion-list")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-widgetized"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-widgetized")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-widget"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-widget")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-widget-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-widget-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-view-list"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-view-list")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-view-list-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-view-list-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-view-grid"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-view-grid")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-upload"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-upload")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-download"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-download")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-loop"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-loop")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-sidebar-2"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-sidebar-2")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-grid4-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-grid4-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-grid3-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-grid3-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-grid2-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-grid2-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-column4-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-column4-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-column3-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-column3-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-layout-column2-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-layout-column2-alt")])])]), _vm._v(" "), _c('div', {
    staticClass: "icon-section"
  }, [_c('h3', [_vm._v("Brand Icons")]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-flickr"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-flickr")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-flickr-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-flickr-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-instagram"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-instagram")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-google"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-google")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-github"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-github")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-facebook"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-facebook")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-dropbox"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-dropbox")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-dropbox-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-dropbox-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-dribbble"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-dribbble")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-apple"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-apple")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-android"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-android")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-yahoo"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-yahoo")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-trello"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-trello")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-stack-overflow"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-stack-overflow")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-soundcloud"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-soundcloud")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-sharethis"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-sharethis")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-sharethis-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-sharethis-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-reddit"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-reddit")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-microsoft"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-microsoft")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-microsoft-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-microsoft-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-linux"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-linux")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-jsfiddle"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-jsfiddle")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-joomla"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-joomla")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-html5"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-html5")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-css3"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-css3")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-drupal"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-drupal")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-wordpress"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-wordpress")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-tumblr"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-tumblr")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-tumblr-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-tumblr-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-skype"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-skype")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-youtube"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-youtube")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-vimeo"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-vimeo")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-vimeo-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-vimeo-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-twitter"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-twitter")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-twitter-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-twitter-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-linkedin"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-linkedin")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-pinterest"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-pinterest")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-pinterest-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-pinterest-alt")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-themify-logo"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-themify-logo")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-themify-favicon"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-themify-favicon")])]), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('span', {
    staticClass: "ti-themify-favicon-alt"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-name"
  }, [_vm._v(" ti-themify-favicon-alt")])])])])])])])
}]}

/***/ }),
/* 138 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card card-user"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "author"
  }, [_c('img', {
    staticClass: "avatar border-white",
    attrs: {
      "src": _vm.profile,
      "alt": "..."
    }
  }), _vm._v(" "), _c('h4', {
    staticClass: "title"
  }, [_vm._v(_vm._s(_vm.name) + "\n        "), _c('br'), _vm._v(" "), _c('a', [_c('small', [_vm._v(_vm._s(_vm.gender))])])])])]), _vm._v(" "), _c('hr'), _vm._v(" "), _c('div', {
    staticClass: "text-center"
  }, [_c('div', {
    staticClass: "row"
  }, _vm._l((_vm.details), function(info, index) {
    return _c('div', {
      class: _vm.getClasses(index)
    }, [_c('h5', [_vm._v(_vm._s(info.title) + "\n          "), _c('br'), _vm._v(" "), _c('small', [_vm._v(_vm._s(info.subTitle))])])])
  }))])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "image"
  }, [_c('img', {
    attrs: {
      "src": "static/img/background.jpg",
      "alt": "..."
    }
  })])
}]}

/***/ }),
/* 139 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "row"
  }, _vm._l((_vm.statsCards), function(stats) {
    return _c('div', {
      staticClass: "col-lg-3 col-sm-6"
    }, [_c('stats-card', [_c('div', {
      staticClass: "icon-big text-center",
      class: ("icon-" + (stats.type)),
      slot: "header"
    }, [_c('i', {
      class: stats.icon
    })]), _vm._v(" "), _c('div', {
      staticClass: "numbers",
      slot: "content"
    }, [_c('p', [_vm._v(_vm._s(stats.title))]), _vm._v("\n          " + _vm._s(stats.value) + "\n        ")]), _vm._v(" "), _c('div', {
      staticClass: "stats",
      slot: "footer"
    }, [_c('i', {
      class: stats.footerIcon
    }), _vm._v(" " + _vm._s(stats.footerText) + "\n        ")])])], 1)
  })), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-12"
  }, [_c('chart-card', {
    attrs: {
      "chart-data": _vm.usersChart.data,
      "chart-options": _vm.usersChart.options
    }
  }, [_c('h4', {
    staticClass: "title",
    slot: "title"
  }, [_vm._v("Users behavior")]), _vm._v(" "), _c('span', {
    slot: "subTitle"
  }, [_vm._v(" 24 Hours performance")]), _vm._v(" "), _c('span', {
    slot: "footer"
  }, [_c('i', {
    staticClass: "ti-reload"
  }), _vm._v(" Updated 3 minutes ago")]), _vm._v(" "), _c('div', {
    slot: "legend"
  }, [_c('i', {
    staticClass: "fa fa-circle text-info"
  }), _vm._v(" Open\n          "), _c('i', {
    staticClass: "fa fa-circle text-danger"
  }), _vm._v(" Click\n          "), _c('i', {
    staticClass: "fa fa-circle text-warning"
  }), _vm._v(" Click Second Time\n        ")])])], 1), _vm._v(" "), _c('div', {
    staticClass: "col-md-6 col-xs-12"
  }, [_c('chart-card', {
    attrs: {
      "chart-data": _vm.preferencesChart.data,
      "chart-type": "Pie"
    }
  }, [_c('h4', {
    staticClass: "title",
    slot: "title"
  }, [_vm._v("Email Statistics")]), _vm._v(" "), _c('span', {
    slot: "subTitle"
  }, [_vm._v(" Last campaign performance")]), _vm._v(" "), _c('span', {
    slot: "footer"
  }, [_c('i', {
    staticClass: "ti-timer"
  }), _vm._v(" Campaign set 2 days ago")]), _vm._v(" "), _c('div', {
    slot: "legend"
  }, [_c('i', {
    staticClass: "fa fa-circle text-info"
  }), _vm._v(" Open\n          "), _c('i', {
    staticClass: "fa fa-circle text-danger"
  }), _vm._v(" Bounce\n          "), _c('i', {
    staticClass: "fa fa-circle text-warning"
  }), _vm._v(" Unsubscribe\n        ")])])], 1), _vm._v(" "), _c('div', {
    staticClass: "col-md-6 col-xs-12"
  }, [_c('chart-card', {
    attrs: {
      "chart-data": _vm.activityChart.data,
      "chart-options": _vm.activityChart.options
    }
  }, [_c('h4', {
    staticClass: "title",
    slot: "title"
  }, [_vm._v("2015 Sales")]), _vm._v(" "), _c('span', {
    slot: "subTitle"
  }, [_vm._v(" All products including Taxes")]), _vm._v(" "), _c('span', {
    slot: "footer"
  }, [_c('i', {
    staticClass: "ti-check"
  }), _vm._v(" Data information certified")]), _vm._v(" "), _c('div', {
    slot: "legend"
  }, [_c('i', {
    staticClass: "fa fa-circle text-info"
  }), _vm._v(" Tesla Model S\n          "), _c('i', {
    staticClass: "fa fa-circle text-warning"
  }), _vm._v(" BMW 5 Series\n        ")])])], 1)])])
},staticRenderFns: []}

/***/ }),
/* 140 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('h1', [_vm._v("Register New Account")]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Username/Email",
      "placeholder": "Username/Email"
    },
    model: {
      value: (_vm.user.email),
      callback: function($$v) {
        _vm.user.email = $$v
      },
      expression: "user.email"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Name",
      "placeholder": "Full Name"
    },
    model: {
      value: (_vm.user.name),
      callback: function($$v) {
        _vm.user.name = $$v
      },
      expression: "user.name"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Gender",
      "placeholder": "Gender"
    },
    model: {
      value: (_vm.user.gender),
      callback: function($$v) {
        _vm.user.gender = $$v
      },
      expression: "user.gender"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Phone",
      "placeholder": "Phone"
    },
    model: {
      value: (_vm.user.phone),
      callback: function($$v) {
        _vm.user.phone = $$v
      },
      expression: "user.phone"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "text",
      "label": "Profile Picture Link",
      "placeholder": "Profile Picture Link"
    },
    model: {
      value: (_vm.user.profile_photo),
      callback: function($$v) {
        _vm.user.profile_photo = $$v
      },
      expression: "user.profile_photo"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('fg-input', {
    attrs: {
      "type": "password",
      "label": "Password",
      "placeholder": "Password"
    },
    model: {
      value: (_vm.user.password),
      callback: function($$v) {
        _vm.user.password = $$v
      },
      expression: "user.password"
    }
  })], 1)]), _vm._v(" "), _c('div', {
    staticClass: "text-center"
  }, [_c('button', {
    staticClass: "btn btn-info btn-fill btn-wd",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.register($event)
      }
    }
  }, [_vm._v("\n      Register\n    ")])]), _vm._v(" "), _c('div', {
    staticClass: "clearfix"
  })])
},staticRenderFns: []}

/***/ }),
/* 141 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "contact-us full-screen"
  }, [_c('div', {
    staticClass: "wrapper wrapper-full-page section content"
  }, [_c('div', {}, [_c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-8 col-md-offset-2 text-center"
  }, [_c('h2', {
    staticClass: "title text-danger"
  }, [_vm._v("404 Not Found")]), _vm._v(" "), _c('h2', {
    staticClass: "title"
  }, [_vm._v("Please don't type in random URLs to test the site, sincerely, Jisan!!")])])])])])])])
}]}

/***/ }),
/* 142 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-xs-5"
  }, [_vm._t("header")], 2), _vm._v(" "), _c('div', {
    staticClass: "col-xs-7"
  }, [_vm._t("content")], 2)]), _vm._v(" "), _c('div', {
    staticClass: "footer"
  }, [_c('hr'), _vm._v(" "), _vm._t("footer")], 2)])])
},staticRenderFns: []}

/***/ }),
/* 143 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card"
  }, [_c('div', {
    staticClass: "container"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group has-danger"
  }, [_c('label', {
    staticClass: "sr-only",
    attrs: {
      "for": "email"
    }
  }, [_vm._v("Username")]), _vm._v(" "), _c('div', {
    staticClass: "input-group mb-2 mr-sm-2 mb-sm-0"
  }, [_vm._m(1), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.email),
      expression: "email"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "name": "email",
      "id": "email",
      "placeholder": "Username/Email",
      "required": "",
      "autofocus": ""
    },
    domProps: {
      "value": (_vm.email)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.email = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _vm._m(2)]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    staticClass: "sr-only",
    attrs: {
      "for": "password"
    }
  }, [_vm._v("Password")]), _vm._v(" "), _c('div', {
    staticClass: "input-group mb-2 mr-sm-2 mb-sm-0"
  }, [_vm._m(3), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.password),
      expression: "password"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "password",
      "name": "password",
      "id": "password",
      "placeholder": "Password",
      "required": ""
    },
    domProps: {
      "value": (_vm.password)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.password = $event.target.value
      }
    }
  })])])]), _vm._v(" "), _vm._m(4)]), _vm._v(" "), _vm._m(5), _vm._v(" "), _c('div', {
    staticClass: "row",
    staticStyle: {
      "padding-top": "1rem"
    }
  }, [_c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('button', {
    staticClass: "btn btn-success",
    attrs: {
      "type": "submit"
    },
    on: {
      "click": _vm.login
    }
  }, [_c('i', {
    staticClass: "fa fa-sign-in"
  }), _vm._v(" Login")]), _vm._v(" "), _c('router-link', {
    staticClass: "btn btn-link",
    attrs: {
      "to": "/app/register"
    }
  }, [_vm._v("Register")])], 1)])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-3"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('h2', [_vm._v("Please Login")]), _vm._v(" "), _c('hr')])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "input-group-addon",
    staticStyle: {
      "width": "2.6rem"
    }
  }, [_c('i', {
    staticClass: "fa fa-at"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('div', {
    staticClass: "form-control-feedback"
  }, [_c('span', {
    staticClass: "text-danger align-middle"
  }, [_c('i', {
    staticClass: "fa fa-close"
  }), _vm._v(" Example error message\n                      ")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "input-group-addon",
    staticStyle: {
      "width": "2.6rem"
    }
  }, [_c('i', {
    staticClass: "fa fa-key"
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-md-3"
  }, [_c('div', {
    staticClass: "form-control-feedback"
  }, [_c('span', {
    staticClass: "text-danger align-middle"
  })])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-3"
  })])
}]}

/***/ }),
/* 144 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "notifications"
  }, [_c('transition-group', {
    attrs: {
      "name": "list"
    }
  }, _vm._l((_vm.notifications), function(notification, index) {
    return _c('notification', {
      key: notification,
      attrs: {
        "message": notification.message,
        "icon": notification.icon,
        "type": notification.type,
        "vertical-align": notification.verticalAlign,
        "horizontal-align": notification.horizontalAlign
      },
      on: {
        "on-close": function($event) {
          _vm.removeNotification(index)
        }
      }
    })
  }))], 1)
},staticRenderFns: []}

/***/ }),
/* 145 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "card"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_vm._m(1), _vm._v(" "), _c('br'), _vm._v(" "), _c('br'), _vm._v(" "), _c('div', {
    staticClass: "places-buttons"
  }, [_vm._m(2), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-3"
  }, [_c('button', {
    staticClass: "btn btn-default btn-block",
    on: {
      "click": function($event) {
        _vm.notifyVue('top', 'left')
      }
    }
  }, [_vm._v("Top Left")])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('button', {
    staticClass: "btn btn-default btn-block",
    on: {
      "click": function($event) {
        _vm.notifyVue('top', 'center')
      }
    }
  }, [_vm._v("Top Center")])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('button', {
    staticClass: "btn btn-default btn-block",
    on: {
      "click": function($event) {
        _vm.notifyVue('top', 'right')
      }
    }
  }, [_vm._v("Top Right")])])]), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-3"
  }, [_c('button', {
    staticClass: "btn btn-default btn-block",
    on: {
      "click": function($event) {
        _vm.notifyVue('bottom', 'left')
      }
    }
  }, [_vm._v("Bottom Left")])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('button', {
    staticClass: "btn btn-default btn-block",
    on: {
      "click": function($event) {
        _vm.notifyVue('bottom', 'center')
      }
    }
  }, [_vm._v("Bottom Center")])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-3"
  }, [_c('button', {
    staticClass: "btn btn-default btn-block",
    on: {
      "click": function($event) {
        _vm.notifyVue('bottom', 'right')
      }
    }
  }, [_vm._v("Bottom Right")])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "header"
  }, [_c('h4', {
    staticClass: "title"
  }, [_vm._v("Notifications")]), _vm._v(" "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Custom Vue notifications plugin")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-6"
  }, [_c('h5', [_vm._v("Notifications Style")]), _vm._v(" "), _c('div', {
    staticClass: "alert alert-info"
  }, [_c('span', [_vm._v("This is a plain notification")])]), _vm._v(" "), _c('div', {
    staticClass: "alert alert-info"
  }, [_c('button', {
    staticClass: "close",
    attrs: {
      "type": "button",
      "aria-hidden": "true"
    }
  }, [_vm._v("×")]), _vm._v(" "), _c('span', [_vm._v("This is a notification with close button.")])]), _vm._v(" "), _c('div', {
    staticClass: "alert alert-info alert-with-icon",
    attrs: {
      "data-notify": "container"
    }
  }, [_c('button', {
    staticClass: "close",
    attrs: {
      "type": "button",
      "aria-hidden": "true"
    }
  }, [_vm._v("×")]), _vm._v(" "), _c('span', {
    staticClass: "ti-bell",
    attrs: {
      "data-notify": "icon"
    }
  }), _vm._v(" "), _c('span', {
    attrs: {
      "data-notify": "message"
    }
  }, [_vm._v("This is a notification with close button and icon.")])]), _vm._v(" "), _c('div', {
    staticClass: "alert alert-info alert-with-icon",
    attrs: {
      "data-notify": "container"
    }
  }, [_c('button', {
    staticClass: "close",
    attrs: {
      "type": "button",
      "aria-hidden": "true"
    }
  }, [_vm._v("×")]), _vm._v(" "), _c('span', {
    staticClass: "ti-pie-chart",
    attrs: {
      "data-notify": "icon"
    }
  }), _vm._v(" "), _c('span', {
    attrs: {
      "data-notify": "message"
    }
  }, [_vm._v("This is a notification with close button and icon and have many lines. You can see that the icon and the close button are always vertically aligned. This is a beautiful notification. So you don't have to worry about the style.")])])]), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('h5', [_vm._v("Notification states")]), _vm._v(" "), _c('div', {
    staticClass: "alert alert-info"
  }, [_c('button', {
    staticClass: "close",
    attrs: {
      "type": "button",
      "aria-hidden": "true"
    }
  }, [_vm._v("×")]), _vm._v(" "), _c('span', [_c('b', [_vm._v(" Info - ")]), _vm._v(" This is a regular notification made with \".alert-info\"")])]), _vm._v(" "), _c('div', {
    staticClass: "alert alert-success"
  }, [_c('button', {
    staticClass: "close",
    attrs: {
      "type": "button",
      "aria-hidden": "true"
    }
  }, [_vm._v("×")]), _vm._v(" "), _c('span', [_c('b', [_vm._v(" Success - ")]), _vm._v(" This is a regular notification made with \".alert-success\"")])]), _vm._v(" "), _c('div', {
    staticClass: "alert alert-warning"
  }, [_c('button', {
    staticClass: "close",
    attrs: {
      "type": "button",
      "aria-hidden": "true"
    }
  }, [_vm._v("×")]), _vm._v(" "), _c('span', [_c('b', [_vm._v(" Warning - ")]), _vm._v(" This is a regular notification made with \".alert-warning\"")])]), _vm._v(" "), _c('div', {
    staticClass: "alert alert-danger"
  }, [_c('button', {
    staticClass: "close",
    attrs: {
      "type": "button",
      "aria-hidden": "true"
    }
  }, [_vm._v("×")]), _vm._v(" "), _c('span', [_c('b', [_vm._v(" Danger - ")]), _vm._v(" This is a regular notification made with \".alert-danger\"")])])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-md-9"
  }, [_c('h5', [_vm._v("Notifications Places\n            "), _c('p', {
    staticClass: "category"
  }, [_vm._v("Click to view notifications")])])])])
}]}

/***/ }),
/* 146 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "moving-arrow",
    style: (_vm.arrowStyle)
  })
},staticRenderFns: []}

/***/ }),
/* 147 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('nav', {
    staticClass: "navbar navbar-default"
  }, [_c('div', {
    staticClass: "container-fluid"
  }, [_c('div', {
    staticClass: "navbar-header"
  }, [_c('button', {
    staticClass: "navbar-toggle",
    class: {
      toggled: _vm.$sidebar.showSidebar
    },
    attrs: {
      "type": "button"
    },
    on: {
      "click": _vm.toggleSidebar
    }
  }, [_c('span', {
    staticClass: "sr-only"
  }, [_vm._v("Toggle navigation")]), _vm._v(" "), _c('span', {
    staticClass: "icon-bar bar1"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-bar bar2"
  }), _vm._v(" "), _c('span', {
    staticClass: "icon-bar bar3"
  })]), _vm._v(" "), _c('a', {
    staticClass: "navbar-brand"
  }, [_vm._v(_vm._s(_vm.routeName))])]), _vm._v(" "), (_vm.id > 1) ? _c('div', {
    staticClass: "navbar-right-menu"
  }, [_c('ul', {
    staticClass: "nav navbar-nav navbar-right"
  }, [_c('li', [_c('a', {
    staticClass: "btn-rotate",
    on: {
      "click": _vm.logOut
    }
  }, [_c('i', {
    staticClass: "ti-na"
  }), _vm._v(" "), _c('p', [_vm._v("\n              Log Out\n            ")])])])]), _vm._v(" "), _c('ul', {
    staticClass: "nav navbar-nav navbar-left"
  }, [_c('li', [_c('a', [_c('p', [_vm._v("\n              " + _vm._s(_vm.name) + "\n            ")])])])])]) : _vm._e()])])
},staticRenderFns: []}

/***/ }),
/* 148 */,
/* 149 */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),
/* 150 */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })
],[49]);
//# sourceMappingURL=app.8eea8e925352ed63a638.js.map