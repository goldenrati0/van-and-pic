from flask import jsonify, make_response, request
from flask_restless import APIManager
from model import app, db, artist, paintings, sculptures
from crud import check_login_credentials, get_id_from_email

api_manager = APIManager(app=app, flask_sqlalchemy_db=db)

api_manager.create_api(artist, methods=['GET', 'POST', 'PUT', 'DELETE'], exclude_columns=['password'],
                       url_prefix='/api/v1', results_per_page=9)
api_manager.create_api(paintings, methods=['GET', 'POST', 'PUT', 'DELETE'], url_prefix='/api/v1', results_per_page=9)
api_manager.create_api(sculptures, methods=['GET', 'POST', 'PUT', 'DELETE'], url_prefix='/api/v1', results_per_page=9)


def add_cors_header(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'HEAD, GET, POST, PATCH, PUT, OPTIONS, DELETE'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type, *'
    response.headers['Access-Control-Allow-Credentials'] = 'true'

    return response


app.after_request(add_cors_header)


@app.route('/')
def home():
    return make_response(
        jsonify(
            {
                "authors": ["safwat", "jisan"],
                "message": "artgallery API",
                "api_version": "1.0.0"
            }
        )
    ), 200


@app.route('/login/', methods=['POST'])
def login():
    try:
        credentials = request.get_json()

        email = credentials['email']
        password = credentials['password']

        if check_login_credentials(email, password):
            return make_response(
                jsonify(
                    {
                        "status": "success",
                        "id": get_id_from_email(email)
                    }
                )
            ), 200
        else:
            return make_response(
                jsonify(
                    {
                        "status": "failed"
                    }
                )
            ), 403
    except:
        return make_response(
            jsonify(
                {
                    "status": "failed"
                }
            )
        ), 403


@app.route('/all_artist')
def all_artist():
    all_artist = artist.query.all()
    all_artist = [artist.json_dump() for artist in all_artist]

    return jsonify(
        {
            "all_artist": all_artist
        }
    )
