import requests
import base64
from crud import get_painting_from_id, get_sculpture_from_id
from model import db

API = 'https://source.unsplash.com/random/700x400'

for i in range(100):
    paint = get_painting_from_id(i)
    if paint is not None:
        res = requests.get(API, allow_redirects=True)
        print("========")
        print(res.url)
        print("========")
        # enc = base64.b64encode(res.content)
        # enc = str(enc.decode("utf-8"))
        paint.photo = res.url
        db.session.commit()

print("============S==============")
for i in range(100):
    paint = get_sculpture_from_id(i)
    if paint is not None:
        res = requests.get(API, allow_redirects=True)
        print("========")
        print(res.url)
        print("========")
        # enc = base64.b64encode(res.content)
        # enc = str(enc.decode("utf-8"))
        paint.photo = res.url
        db.session.commit()
