import requests, json
from model import db, artist

RANDOM_USER_API = 'https://randomuser.me/api/?results='
NUMBER_OF_USERS = 30

api_response = requests.get(RANDOM_USER_API + str(NUMBER_OF_USERS) + '&nat=gb')
json_data = json.loads(api_response.text)

user_list = json_data['results']

for user in user_list:
    gender = user['gender'].capitalize()
    name = user['name']['first'].capitalize() + ' ' + user['name']['last'].capitalize()
    email = user['email']
    username = user['login']['username']
    password = user['login']['password']
    dob = user['dob']
    phone = user['cell']
    picture_large = user['picture']['large']
    picture_medium = user['picture']['medium']

    new_artist = artist(email, name, password, phone, gender, picture_large)

    db.session.add(new_artist)
    db.session.commit()

    print(
        name + '\n' + gender + '\n' + dob + '\n' + email + '\n' + username + '\n' + phone + '\n' + password + '\n' + picture_large + '\n' + picture_medium + '\n===============================================\n')

"""
import json
from model import db, artist

all_artists = json.loads(open('test_data_artist.json').read())
all_artists = all_artists['artist']

for data in all_artists:
    email = data['email']
    name = data['name']
    password = data['password']
    phone = data['phone']

    new_artist = artist(email, name, password, phone)

    db.session.add(new_artist)
    db.session.commit()

    print(name + " added")
"""
