import json
import requests
import base64
from model import db, paintings


def base64_photo():
    API = 'https://source.unsplash.com/random/700x400'

    res = requests.get(API, allow_redirects=True)
    print("========")
    print(res.url)
    print("========")
    # enc = base64.b64encode(res.content)
    # enc = str(enc.decode("utf-8"))

    return str(res.url)


all_painting = json.loads(open('test_data_painting.json').read())
all_painting = all_painting['painting']

for data in all_painting:
    artist_id = data['artist_id']
    name = data['name']
    description = data['description']
    photo = base64_photo()

    new_artist = paintings(artist_id, name, photo, description)

    db.session.add(new_artist)
    db.session.commit()

    print(name + " added")
